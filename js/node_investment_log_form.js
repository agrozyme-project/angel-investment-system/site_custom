"use strict";
(function ($) {
    // @ts-ignore
    Drupal.behaviors.site_custom = {
        attach: (context, settings) => {
            const $document = $(document);
            // @ts-ignore
            $document.once("site_custom").each(() => {
                const local = settings["site_custom/node_investment_log_form"];
                const { groups, signs, prefixes, defaultReason } = local;
                const review = $("#edit-investment-review");
                const reason = $("#edit-investment-reason");
                // console.dir(local);
                const getPrefix = (sign, orign) => {
                    let item = orign;
                    if (0 > sign) {
                        item += "-";
                    }
                    else if (0 < sign) {
                        item += "+";
                    }
                    else {
                        item += "&nbsp;";
                    }
                    return item;
                };
                reason.on("change", () => {
                    const index = reason.val();
                    if (false === signs.hasOwnProperty(index)) {
                        return;
                    }
                    $.each(signs[index], (signField, signValue) => {
                        const color = 0 > signValue ? "red" : "";
                        $.each(groups[signField], (inputField, inputFormat) => {
                            const prefix = $(".form-item-" +
                                inputField.replace(/_/g, "-") +
                                "-0-value .field-prefix");
                            prefix.html(getPrefix(signValue, prefixes[inputField]));
                            prefix.css("display", "inline-block");
                            prefix.css("width", "1em");
                            prefix.css("margin-right", "1ex");
                            prefix.css("color", color);
                        });
                    });
                });
                reason.trigger("change");
                review.on("change", () => {
                    const disabled = "_none" !== review.val();
                    if (disabled) {
                        reason.val(defaultReason).trigger("change");
                    }
                    reason.prop("disabled", disabled).trigger("chosen:updated");
                });
                review.trigger("change");
            });
        }
    };
})(jQuery);
