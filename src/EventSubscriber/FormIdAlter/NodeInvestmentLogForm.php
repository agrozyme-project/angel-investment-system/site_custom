<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\FormIdAlter;

use Drupal;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent as Event;
use Drupal\field\Entity\FieldConfig;
use Drupal\site_custom\Helper\InvestmentReason;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class NodeInvestmentLogForm
 */
class NodeInvestmentLogForm implements EventSubscriberInterface
{
  protected $signs = [];

  protected $prefixes = [];

  protected $defaultReason = 0;

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.form_node_investment_log_form.alter' => 'alter',
      'hook_event_dispatcher.form_node_investment_log_edit_form.alter' => 'alter',
    ];
  }

  /**
   * @param Event $event
   */
  function alter(Event $event)
  {
    $this->signs = $this->setupSigns();
    $this->prefixes = $this->setupPrefixes();
    $this->defaultReason = $this->setupDefaultReason();

    $this->alterInvestmentReason($event);
    $this->alterDisabledFields($event);
    $this->attachedSign($event);
  }

  /**
   * @return array
   */
  protected function setupSigns()
  {
    $items = [];

    /** @var Term[] $terms */
    $terms = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'investment_reason']);
    $reasons = array_keys(InvestmentReason::getSignMap());

    foreach ($terms as $index => $term) {
      foreach ($reasons as $reason) {
        //  TODO: check empty first()
        $value = $term
          ->get($reason)
          ->first()
          ->getValue();
        $items[$index][$reason] = intval($value['value']);
      }
    }

    return $items;
  }

  /**
   * @return array
   */
  protected function setupPrefixes()
  {
    $items = [];

    /** @var EntityFieldManager $manager */
    $manager = Drupal::getContainer()->get('entity_field.manager');
    $data = $manager->getFieldDefinitions('node', 'investment_log');

    foreach (InvestmentReason::getSignMap() as $fields) {
      foreach (array_keys($fields) as $field) {
        /** @var FieldDefinitionInterface $item */
        $item = $data[$field];
        $items[$field] = $item->getSetting('prefix');
      }
    }

    return $items;
  }

  /**
   * @return int
   */
  protected function setupDefaultReason()
  {
    $config = FieldConfig::loadByName('node', 'investment_log', 'investment_reason');
    $value = $config->get('default_value');
    $uuid = $value[0]['target_uuid'] ?? '';
    $keys = array_keys(
      Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['uuid' => $uuid])
    );

    if (empty($keys)) {
      return 0;
    }

    $key = reset($keys);
    return $key;
  }

  /**
   * @param Event $event
   */
  protected function alterInvestmentReason(Event $event)
  {
    $form = &$event->getForm();

    if (empty($form['investment_review']['widget']['#default_value'])) {
      return;
    }

    $key = $this->defaultReason;

    if (false === empty($key)) {
      $field = &$form['investment_reason']['widget'];
      $field['#default_value'] = [$key];
      $field['#disabled'] = true;
    }
  }

  /**
   * @param Event $event
   */
  protected function alterDisabledFields(Event $event)
  {
    $form = &$event->getForm();

    foreach ($this->signs as $index => $sign) {
      foreach (InvestmentReason::getSignMap() as $reason => $fields) {
        if (0 !== $sign[$reason]) {
          continue;
        }

        foreach (array_keys($fields) as $field) {
          $form[$field]['#states']['disabled'][] = [
            '#edit-investment-reason' => ['value' => $index],
          ];
        }
      }
    }
  }

  /**
   * @param Event $event
   */
  protected function attachedSign(Event $event)
  {
    $form = &$event->getForm();
    $groups = InvestmentReason::getSignMap();
    $signs = $this->signs;
    $prefixes = $this->prefixes;
    $defaultReason = reset($form['investment_reason']['widget']['#default_value']);

    $name = 'site_custom/node_investment_log_form';
    $form['#attached']['drupalSettings'][$name] = compact('groups', 'signs', 'prefixes', 'defaultReason');
    $form['#attached']['library'][] = $name;
  }
}
