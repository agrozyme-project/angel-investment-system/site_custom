<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\field_event_dispatcher\Event\Field\WidgetFormAlterEvent as Event;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class WidgetFormAlter
 */
class WidgetFormAlter implements EventSubscriberInterface
{
  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [HookEventDispatcherInterface::WIDGET_FORM_ALTER => 'alter'];
  }

  /**
   * @param Event $event
   */
  function alter(Event $event)
  {
    $context = $event->getContext();

    /** @var FieldItemListInterface $list */
    $list = $context['items'];

    if ('decimal' === $list->getFieldDefinition()->getType()) {
      $element = &$event->getElement();
      $element['value']['#step'] = 'any';
    }
  }
}
