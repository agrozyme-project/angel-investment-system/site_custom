<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DebtRatio
 */
class DebtRatio extends Base implements EventSubscriberInterface
{
  protected $total_liability = 0;
  protected $total_asset = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $data = $item;
    $fields = [
      FieldReader::create($data, 'total_liability', 'value'),
      FieldReader::create($data, 'total_asset', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.debt_ratio.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.debt_ratio.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $total_asset = Decimal::create($this->total_asset, $scale);

    if ($total_asset->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->total_liability, $scale)->div($total_asset);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
