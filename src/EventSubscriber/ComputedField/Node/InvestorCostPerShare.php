<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\InvestorSubtotalShare as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class InvestorCostPerShare
 */
class InvestorCostPerShare extends Base implements EventSubscriberInterface
{
  protected $investor_investment_amount = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $items = parent::getValues($data);
    $fields = [FieldReader::create($data, 'investor_investment_amount', 'value')];
    return $items + FieldReader::getValues($fields);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.investor_cost_per_share.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.investor_cost_per_share.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $investor_subtotal_share = Decimal::create($computeEvent->getValue(), $scale);

    if ($investor_subtotal_share->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->investor_investment_amount, $scale)->div($investor_subtotal_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
