<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\FunderSubtotalShare as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FunderShareholdingRatio
 */
class FunderShareholdingRatio extends Base implements EventSubscriberInterface
{
  protected $total_share = 0;

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.funder_shareholding_ratio.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.funder_shareholding_ratio.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $funder_subtotal_share = $computeEvent->getValue();
    $total_share = Decimal::create($this->total_share, $scale);

    if ($total_share->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($funder_subtotal_share, $scale)->div($total_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $items = parent::getValues($item);
    $data = $item;
    $fields = [FieldReader::create($data, 'total_share', 'value')];
    return $items + FieldReader::getValues($fields);
  }
}
