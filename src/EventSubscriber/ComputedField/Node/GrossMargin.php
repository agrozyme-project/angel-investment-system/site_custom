<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\GrossProfit as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class GrossMargin
 */
class GrossMargin extends Base implements EventSubscriberInterface
{
  protected $net_operating_revenue = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $items = parent::getValues($item);
    $data = $item;
    $fields = [FieldReader::create($data, 'net_operating_revenue', 'value')];
    return $items + FieldReader::getValues($fields);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.gross_margin.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.gross_margin.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $gross_profit = $computeEvent->getValue();
    $net_operating_revenue = Decimal::create($this->net_operating_revenue, $scale);

    if ($net_operating_revenue->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($gross_profit, $scale)->div($net_operating_revenue);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
