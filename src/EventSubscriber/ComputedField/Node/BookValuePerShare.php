<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\TotalEquity as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BookValuePerShare
 */
class BookValuePerShare extends Base implements EventSubscriberInterface
{
  protected $total_common_share = 0;

  protected $total_preferred_share = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $items = parent::getValues($item);
    $data = $item;
    $fields = [
      FieldReader::create($data, 'total_common_share', 'value'),
      FieldReader::create($data, 'total_preferred_share', 'value')
    ];
    return $items + FieldReader::getValues($fields);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.book_value_per_share.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.book_value_per_share.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $total_equity = Decimal::create($computeEvent->getValue(), $scale);
    $total_shares = Decimal::create($this->total_common_share, $scale)->add($this->total_preferred_share);

    if ($total_shares->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($total_equity, $scale)->div($total_shares);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
