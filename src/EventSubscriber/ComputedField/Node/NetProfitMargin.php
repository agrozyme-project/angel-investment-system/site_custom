<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class NetProfitMargin
 */
class NetProfitMargin extends Base implements EventSubscriberInterface
{
  protected $net_income = 0;
  protected $net_operating_revenue = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $fields = [
      FieldReader::create($data, 'net_income', 'value'),
      FieldReader::create($data, 'net_operating_revenue', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.net_profit_margin.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.net_profit_margin.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $net_operating_revenue = Decimal::create($this->net_operating_revenue, $scale);

    if ($net_operating_revenue->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->net_income, $scale)->div($net_operating_revenue);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
