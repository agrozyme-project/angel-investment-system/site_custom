<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TotalEquity
 */
class TotalEquity extends Base implements EventSubscriberInterface
{
  protected $total_asset = 0;
  protected $total_liability = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $fields = [
      FieldReader::create($data, 'total_asset', 'value'),
      FieldReader::create($data, 'total_liability', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.total_equity.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.total_equity.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->total_asset, $scale)->sub($this->total_liability);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
