<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class GrossProfit
 */
class GrossProfit extends Base implements EventSubscriberInterface
{
  protected $net_operating_revenue = 0;
  protected $operating_cost = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $fields = [
      FieldReader::create($data, 'net_operating_revenue', 'value'),
      FieldReader::create($data, 'operating_cost', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.gross_profit.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.gross_profit.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->net_operating_revenue, $scale)->sub($this->operating_cost);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
