<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ShareholdingRatio
 */
class ShareholdingRatio extends Base implements EventSubscriberInterface
{
  protected $shareholding_quantity = 0;
  protected $total_share = 0;

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.shareholding_ratio.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.shareholding_ratio.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $total_share = Decimal::create($this->total_share, $scale);

    if ($total_share->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->shareholding_quantity, $scale)->div($total_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $items = [];

    $item = $data;
    $fields = [
      FieldReader::create($item, 'shareholding_quantity', 'value'),
      FieldReader::create($item, 'total_share', 'value')
    ];

    $items += FieldReader::getValues($fields);
    return $items;
  }
}
