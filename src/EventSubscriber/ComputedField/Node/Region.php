<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldFormatEvent as FormatEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use Drupal\site_custom\TaiwanRegion;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class Region
 */
class Region extends Base implements EventSubscriberInterface
{
  protected $address = ['country_code' => '', 'administrative_area' => ''];

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $fields = [FieldReader::create($data, 'address', '')];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.region.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.region.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = TaiwanRegion::getInstance()->getRegion($this->address);
    $computeEvent->setValue($value);
    return true;
  }

  /**
   * {@inheritdoc}
   */
  function format(FormatEvent $formatEvent): bool
  {
    if (false === parent::format($formatEvent)) {
      $formatEvent->setValue('');
      return false;
    }

    $value = TaiwanRegion::getInstance()
      ->getName($formatEvent->getValue())
      ->render();
    $formatEvent->setValue($value);
    return true;
  }
}
