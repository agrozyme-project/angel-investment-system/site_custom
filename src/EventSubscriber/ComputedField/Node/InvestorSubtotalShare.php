<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class InvestorSubtotalShare
 */
class InvestorSubtotalShare extends Base implements EventSubscriberInterface
{
  protected $investor_preferred_share = 0;
  protected $investor_common_share = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $fields = [
      FieldReader::create($data, 'investor_preferred_share', 'value'),
      FieldReader::create($data, 'investor_common_share', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.investor_subtotal_share.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.investor_subtotal_share.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->investor_preferred_share, $scale)->add($this->investor_common_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
