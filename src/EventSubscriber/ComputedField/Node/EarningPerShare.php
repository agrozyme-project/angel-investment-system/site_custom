<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EarningPerShare
 */
class EarningPerShare extends Base implements EventSubscriberInterface
{
  protected $net_income = 0;
  protected $preferred_share_dividend = 0;
  protected $total_common_share = 0;
  protected $total_preferred_share = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $data = $item;
    $fields = [
      FieldReader::create($data, 'net_income', 'value'),
      FieldReader::create($data, 'preferred_share_dividend', 'value'),
      FieldReader::create($data, 'total_common_share', 'value'),
      FieldReader::create($data, 'total_preferred_share', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.earning_per_share.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.earning_per_share.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $total_shares = Decimal::create($this->total_common_share, $scale)->add($this->total_preferred_share);

    if ($total_shares->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->net_income, $scale)
      ->sub($this->preferred_share_dividend)
      ->div($total_shares);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
