<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\InvestorSubtotalShare as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class InvestorShareholdingRatio
 */
class InvestorShareholdingRatio extends Base implements EventSubscriberInterface
{
  protected $total_share = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $items = parent::getValues($data);
    $fields = [FieldReader::create($data, 'total_share', 'value')];
    return $items + FieldReader::getValues($fields);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents(): array
  {
    return [
      'hook_event_dispatcher.computed_field.node.investor_shareholding_ratio.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.investor_shareholding_ratio.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $investor_subtotal_share = $computeEvent->getValue();
    $total_share = Decimal::create($this->total_share, $scale);

    if ($total_share->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($investor_subtotal_share, $scale)->div($total_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
