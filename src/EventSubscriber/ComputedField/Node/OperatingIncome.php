<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\GrossProfit as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OperatingIncome
 */
class OperatingIncome extends Base implements EventSubscriberInterface
{
  protected $operating_expense = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $items = parent::getValues($data);
    $fields = [FieldReader::create($data, 'operating_expense', 'value')];
    return $items + FieldReader::getValues($fields);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.operating_income.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.operating_income.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $gross_profit = $computeEvent->getValue();
    $value = Decimal::create($gross_profit, $scale)->sub($this->operating_expense);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
