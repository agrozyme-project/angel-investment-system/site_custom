<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FunderSubtotalShare
 */
class FunderSubtotalShare extends Base implements EventSubscriberInterface
{
  protected $funder_preferred_share = 0;
  protected $funder_common_share = 0;

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.funder_subtotal_share.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.funder_subtotal_share.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->funder_preferred_share, $scale)->add($this->funder_common_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $data = $item;
    $fields = [
      FieldReader::create($data, 'funder_preferred_share', 'value'),
      FieldReader::create($data, 'funder_common_share', 'value')
    ];
    return FieldReader::getValues($fields);
  }
}
