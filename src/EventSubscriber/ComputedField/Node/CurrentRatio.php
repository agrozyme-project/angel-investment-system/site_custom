<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CurrentRatio
 */
class CurrentRatio extends Base implements EventSubscriberInterface
{
  protected $current_asset = 0;
  protected $current_liability = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $data = $item;
    $fields = [
      FieldReader::create($data, 'current_asset', 'value'),
      FieldReader::create($data, 'current_liability', 'value')
    ];
    return FieldReader::getValues($fields);
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.current_ratio.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.current_ratio.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $current_liability = Decimal::create($this->current_liability, $scale);

    if ($current_liability->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->current_asset, $scale)->div($current_liability);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
