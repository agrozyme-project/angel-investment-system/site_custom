<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\EventSubscriber\ComputedField\Node\FunderSubtotalShare as Base;
use Drupal\site_custom\Helper\FieldReader;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FunderCostPerShare
 */
class FunderCostPerShare extends Base implements EventSubscriberInterface
{
  protected $funder_investment_amount = 0;

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $item): array
  {
    $items = parent::getValues($item);
    $data = $item;
    $fields = [FieldReader::create($data, 'funder_investment_amount', 'value')];
    return $items + FieldReader::getValues($fields);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function getSubscribedEvents()
  {
    return [
      'hook_event_dispatcher.computed_field.node.funder_cost_per_share.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.funder_cost_per_share.format' => 'format'
    ];
  }

  /**
   * {@inheritdoc}
   */
  function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $funder_subtotal_share = Decimal::create($computeEvent->getValue(), $scale);

    if ($funder_subtotal_share->eq(0)) {
      $computeEvent->setValue('');
      return false;
    }

    $value = Decimal::create($this->funder_investment_amount, $scale)->div($funder_subtotal_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }
}
