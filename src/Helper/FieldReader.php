<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper;

use Drupal\Core\TypedData\ComplexDataInterface;

/**
 * Class FieldReader
 */
class FieldReader
{
  protected $data;
  protected $name = '';
  protected $index = '';
  protected $delta = 0;

  /**
   * FieldParameter constructor.
   *
   * @param ComplexDataInterface $data
   * @param string               $name
   * @param string               $index
   * @param int                  $delta
   */
  protected function __construct(ComplexDataInterface $data, string $name, string $index = '', int $delta = 0)
  {
    $this->data = $data;
    $this->name = $name;
    $this->index = $index;
    $this->delta = $delta;
  }

  /**
   * @param ComplexDataInterface $data
   * @param string               $name
   * @param string               $index
   * @param int                  $delta
   *
   * @return static
   */
  static function create(ComplexDataInterface $data, string $name, string $index = '', int $delta = 0)
  {
    return new static($data, $name, $index, $delta);
  }

  /**
   * @param FieldReader[] $items
   *
   * @return array
   */
  static function getValues(array $items = [])
  {
    $data = [];

    foreach ($items as $item) {
      $data[$item->getName()] = $item->getValue();
    }

    return $data;
  }

  /**
   * @return mixed
   */
  function getValue()
  {
    $index = $this->index;
    $value = $this->data->get($this->name)->getValue();
    $value = $value[$this->delta] ?? null;

    if ('' === $index) {
      return $value;
    }

    $value = $value[$index] ?? null;
    return $value;
  }

  /**
   * @return bool
   */
  function isEmpty(): bool
  {
    $value = $this->getValue();
    return isset($value);
  }

  /**
   * @return string
   */
  function getName(): string
  {
    return $this->name;
  }
}
