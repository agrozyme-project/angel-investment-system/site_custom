<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class DatePeriod
 */
class DatePeriod
{
  protected $date;

  /**
   * @param DrupalDateTime $date
   */
  function __construct(DrupalDateTime $date)
  {
    $this->date = $date;
  }

  /**
   * @return int
   */
  function getYear(): int
  {
    return intval($this->date->format('Y'));
  }

  /**
   * @return int
   */
  function getMonth(): int
  {
    return intval($this->date->format('m'));
  }

  /**
   * @return int
   */
  function getQuarter(): int
  {
    $month = $this->getMonth();
    return intval(floor($month / 4) + 1);
  }
}
