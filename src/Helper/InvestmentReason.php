<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use RtLopez\Decimal;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentReason
 */
class InvestmentReason
{
  const AMOUNT = 'amount';

  const NUMBER = 'number';
  protected static $entityItems = [];
  protected static $fieldMap = [];
  protected static $signMap = [];
  protected $entity;
  protected $cash_dividend_sign = 0;
  protected $common_amount_sign = 0;
  protected $common_stock_sign = 0;
  protected $interest_income_sign = 0;
  protected $preferred_amount_sign = 0;
  protected $preferred_stock_sign = 0;
  protected $signs = [];

  /**
   * InvestmentReason constructor.
   *
   * @param EntityInterface $entity
   */
  protected function __construct(EntityInterface $entity)
  {
    $this->entity = $entity->getTypedData();

    foreach (array_keys(static::getSignMap()) as $field) {
      $this->{$field} = intval($this->entity->get($field)->getString());
    }
  }

  /**
   * @return array
   */
  static function getSignMap(): array
  {
    $map = static::$signMap;

    if (false === empty($map)) {
      return $map;
    }

    $map = [
      'investment_amount_sign' => [
        'funder_investment_amount' => static::AMOUNT,
        'investor_investment_amount' => static::AMOUNT
      ],
      'common_amount_sign' => [
        'funder_common_amount' => static::AMOUNT,
        'investor_common_amount' => static::AMOUNT
      ],
      'common_share_sign' => [
        'funder_common_share' => static::NUMBER,
        'investor_common_share' => static::NUMBER
      ],
      'preferred_amount_sign' => [
        'funder_preferred_amount' => static::AMOUNT,
        'investor_preferred_amount' => static::AMOUNT
      ],
      'preferred_share_sign' => [
        'funder_preferred_share' => static::NUMBER,
        'investor_preferred_share' => static::NUMBER
      ],
      'cash_dividend_sign' => [
        'funder_cash_dividend' => static::AMOUNT
      ],
      'interest_income_sign' => [
        'funder_interest_income' => static::AMOUNT
      ]
    ];

    static::$signMap = $map;
    return $map;
  }

  /**
   * @return array
   */
  static function getFieldMap(): array
  {
    $map = static::$fieldMap;

    if (false === empty($map)) {
      return $map;
    }

    foreach (static::getSignMap() as $sign => $fields) {
      foreach (array_keys($fields) as $field) {
        $map[$field] = $sign;
      }
    }

    static::$fieldMap = $map;
    return $map;
  }

  /**
   * @return static[]
   */
  static function getEntityItems(): array
  {
    $items = static::$entityItems;

    if (false === empty($items)) {
      return $items;
    }

    $terms = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'investment_reason']);

    foreach ($terms as $index => $term) {
      $items[$index] = new static($term);
    }

    static::$entityItems = $items;

    return $items;
  }

  /**
   * @param array $names
   *
   * @return static[]
   */
  static function getItemsByName(array $names = []): array
  {
    $items = Collection::make(static::getEntityItems());

    return $items
      ->filter(function ($item) use ($names) {
        /** @var static $item */
        $name = $item->entity->getString();
        return in_array($name, $names);
      })
      ->toArray();
  }

  /**
   * @param EntityInterface[] $items
   * @param array             $allowed
   *
   * @return Decimal[]
   */
  static function sum(array $items, array $allowed = [])
  {
    /** @var Decimal[] $data */
    $data = static::getEmptySumItem();
    $computeItems = static::computeItems($items, $allowed);

    foreach (array_keys($data) as $field) {
      foreach ($computeItems as $item) {
        if (false === isset($item[$field])) {
          continue;
        }

        $data[$field] = $data[$field]->add($item[$field]);
      }
    }

    return $data;
  }

  /**
   * @return Decimal[]
   */
  static function getEmptySumItem()
  {
    /** @var Decimal[] $data */
    $data = [];

    foreach (array_keys(static::getFieldMap()) as $field) {
      $data[$field] = Decimal::create(0);
    }

    return $data;
  }

  /**
   * @param EntityInterface[] $items
   * @param array             $allowed
   *
   * @return Decimal[]
   */
  protected static function computeItems(array $items, array $allowed = []): array
  {
    $reasons = static::getEntityItems();

    return array_map(function ($item) use ($reasons) {
      /** @var EntityInterface $item */
      $key = intval(
        $item
          ->getTypedData()
          ->get('investment_reason')
          ->getString()
      );

      $reason = $reasons[$key];
      return $reason->compute($item);
    }, static::filterInvestmentLogs($items, $allowed));
  }

  /**
   * @param EntityInterface[] $items
   * @param array             $allowed
   *
   * @return EntityInterface[]
   */
  protected static function filterInvestmentLogs(array $items, array $allowed = []): array
  {
    $reasons = static::getEntityItems();

    return array_filter($items, function ($item) use ($reasons, $allowed) {
      if (false === $item instanceof EntityInterface) {
        return false;
      }

      /** @var EntityInterface $item */
      if ('node' !== $item->getEntityTypeId()) {
        return false;
      }

      if ('investment_log' !== $item->bundle()) {
        return false;
      }

      $data = $item->toArray();

      if (false === isset($data['investment_reason'])) {
        return false;
      }

      $key = intval(
        $item
          ->getTypedData()
          ->get('investment_reason')
          ->getString()
      );

      if (false === empty($allowed) && false === in_array($key, $allowed)) {
        return false;
      }

      return isset($reasons[$key]);
    });
  }

  /**
   * @param string $field
   *
   * @return int
   */
  function get(string $field): int
  {
    $map = static::getSignMap();
    return isset($map[$field]) ? $this->{$field} : 0;
  }

  /**
   * @return int[]
   */
  function getItems(): array
  {
    $items = [];

    foreach (static::getSignMap() as $field) {
      $items[$field] = $this->{$field};
    }

    return $items;
  }

  /**
   * @return ComplexDataInterface
   */
  function getEntity(): ComplexDataInterface
  {
    return $this->entity;
  }

  /**
   * @return int
   */
  function getCashDividendSign(): int
  {
    return $this->cash_dividend_sign;
  }

  /**
   * @return int
   */
  function getCommonAmountSign(): int
  {
    return $this->common_amount_sign;
  }

  /**
   * @return int
   */
  function getCommonStockSign(): int
  {
    return $this->common_stock_sign;
  }

  /**
   * @return int
   */
  function getInterestIncomeSign(): int
  {
    return $this->interest_income_sign;
  }

  /**
   * @return int
   */
  function getPreferredAmountSign(): int
  {
    return $this->preferred_amount_sign;
  }

  /**
   * @return int
   */
  function getPreferredStockSign(): int
  {
    return $this->preferred_stock_sign;
  }

  /**
   * @param EntityInterface $entity
   *
   * @return array
   */
  function compute(EntityInterface $entity): array
  {
    $data = $entity->toArray();
    $result = [];

    foreach (static::getFieldMap() as $field => $sign) {
      $result[$field] = isset($data[$field])
        ? Decimal::create(
          $entity
            ->getTypedData()
            ->get($field)
            ->getString()
        )->mul($this->get($sign))
        : Decimal::create(0);
    }

    return $result;
  }
}
