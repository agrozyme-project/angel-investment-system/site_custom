<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Entity\Node;

use Drupal\site_custom\Helper\Entity\Node;

/**
 * Class InvestmentLog
 */
class InvestmentLog extends Node
{
  protected $bundle = 'investment_log';

  /**
   * @inheritDoc
   */
  function getDefaultValues(): array
  {
    $values = ['total_share' => 0];
    return $values + parent::getDefaultValues();
  }
}
