<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Entity\Node;

use Drupal\site_custom\Helper\Entity\Node;

/**
 * Class InvestmentReview
 */
class InvestmentReview extends Node
{
  protected $bundle = 'investment_review';
}
