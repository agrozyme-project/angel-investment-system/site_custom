<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Entity\Node;

use Drupal\site_custom\Helper\Entity\Node;

/**
 * Class FinancialSummary
 */
class FinancialSummary extends Node
{
  protected $bundle = 'financial_summary';

  /**
   * @inheritDoc
   */
  function getDefaultValues(): array
  {
    $values = [
      'net_operating_revenue' => 0,
      'net_income' => 0,
      'book_value_per_share' => 0,
      'market_value_per_share' => 0
    ];
    return $values + parent::getDefaultValues();
  }
}
