<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Entity\Node;

use Drupal\site_custom\Helper\Entity\Node;

/**
 * Class InvestmentProfile
 */
class InvestmentProfile extends Node
{
  protected $bundle = 'investment_profile';

  /**
   * @inheritDoc
   */
  function getDefaultValues(): array
  {
    $values = [
      'funder_seat' => 0
    ];
    return $values + parent::getDefaultValues();
  }
}
