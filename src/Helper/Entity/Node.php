<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Entity;

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Class Node
 */
abstract class Node extends EntityBase
{
  protected $type = 'node';

  /**
   * @return static
   */
  static function create()
  {
    return new static();
  }

  /**
   * @return array
   */
  function getDefaultValues(): array
  {
    $values = ['type' => $this->bundle];
    return $values + parent::getDefaultValues();
  }

  /**
   * @return QueryInterface
   */
  function getQuery(): QueryInterface
  {
    return parent::getQuery()
      ->condition('type', $this->bundle, '=')
      ->condition('status', 1, '=');
  }
}
