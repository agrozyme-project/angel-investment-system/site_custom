<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Entity;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Class Entity
 */
abstract class EntityBase
{
  protected $type = '';

  protected $bundle = '';

  /**
   * Entity constructor.
   */
  function __construct()
  {
  }

  /**
   * @return array
   */
  function getDefaultValues(): array
  {
    return [];
  }

  /**
   * @return EntityStorageInterface
   */
  function getEntityStorage()
  {
    return Drupal::entityTypeManager()->getStorage($this->type);
  }

  /**
   * @return EntityInterface
   */
  function getEmptyEntity(): EntityInterface
  {
    $values = $this->getDefaultValues();
    return $this->getEntityStorage()->create($values);
  }

  /**
   * @param array $ids
   *
   * @return EntityInterface
   */
  function getFirstOrEmptyEntity(array $ids)
  {
    $items = $this->getEntityStorage()->loadMultiple($ids);
    return empty($items) ? $this->getEmptyEntity() : reset($items);
  }

  /**
   * @return QueryInterface
   */
  function getQuery(): QueryInterface
  {
    return Drupal::entityQuery($this->type);
  }

  /**
   * @param string $field
   *
   * @return QueryInterface
   */
  function getLastQuery(string $field)
  {
    return $this->getQuery()
      ->sort($field, 'DESC')
      ->range(0, 1);
  }
}
