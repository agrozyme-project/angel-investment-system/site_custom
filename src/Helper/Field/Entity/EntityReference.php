<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Entity;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\site_custom\Helper\Field\Base\EntityField;
use Tightenco\Collect\Support\Collection;

/**
 * Class EntityReference
 */
class EntityReference extends EntityField
{
  /**
   * @noinspection PhpMissingParentCallCommonInspection
   *
   * @param array $format
   *
   * @return string
   */
  function toString(array $format = []): string
  {
    $entities = $this->loadTranslationEntities();
    $labels = $this->getLabels($entities);
    return join("<br />\n", $labels);
  }

  /**
   * @return string
   */
  protected function getTargetType(): string
  {
    return $this->value->getDataDefinition()->getSetting('target_type');
  }

  /**
   * @return string
   */
  protected function getLanguageCode(): string
  {
    return Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
  }

  /**
   * @return string[]
   */
  protected function getTargets(): array
  {
    $value = $this->value->getValue();
    $items = Collection::make($value)->map(function ($item) {
      return $item['target_id'];
    });

    return $items->toArray();
  }

  /**
   * @return EntityInterface[]
   */
  protected function loadEntities(): array
  {
    $type = $this->getTargetType();
    $targets = $this->getTargets();

    return Drupal::entityTypeManager()->getStorage($type)->loadMultiple($targets);
  }

  /**
   * @return EntityInterface[]
   */
  protected function loadTranslationEntities(): array
  {
    /** @var EntityRepositoryInterface $repository */
    $repository = Drupal::service('entity.repository');
    $code = $this->getLanguageCode();
    $entities = $this->loadEntities();

    $items = Collection::make($entities)->map(function ($item) use ($repository, $code) {
      return $repository->getTranslationFromContext($item, $code);
    });

    return $items->toArray();
  }

  /**
   * @param EntityInterface[] $entities
   *
   * @return string[]
   */
  protected function getLabels(array $entities): array
  {
    $items = Collection::make($entities)->map(function (EntityInterface $item) {
      return $item->label();
    });

    return $items->toArray();
  }
}
