<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Entity;

use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\site_custom\Helper\Field\Value\IntegerNumber;
use RtLopez\Decimal;

/**
 * Class IntegerField
 */
class IntegerField extends IntegerNumber
{
  /**
   * @param TypedDataInterface $value
   * @param array              $format
   */
  function __construct(TypedDataInterface $value, array $format = [])
  {
    $item = $value->getString();
    parent::__construct(Decimal::create($item), $format);
  }
}
