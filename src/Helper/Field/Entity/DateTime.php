<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\site_custom\Helper\Field\Value\DateTime as Base;

/**
 * Class DateTime
 */
class DateTime extends Base
{
  /**
   * @param TypedDataInterface $value
   * @param array              $format
   */
  function __construct(TypedDataInterface $value, array $format = [])
  {
    $item = empty($value->getValue()) ? 0 : $value->getString();
    parent::__construct(new DrupalDateTime($item), $format);

    if ('date' === $value->getDataDefinition()->getSetting('datetime_type')) {
      $this->format['format'] = DateTimeItemInterface::DATE_STORAGE_FORMAT;
    }
  }
}
