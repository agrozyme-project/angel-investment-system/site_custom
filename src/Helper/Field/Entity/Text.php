<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Entity;

use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\site_custom\Helper\Field\Value\Text as Base;

/**
 * Class Text
 */
class Text extends Base
{
  /**
   * @param TypedDataInterface $value
   * @param array              $format
   */
  function __construct(TypedDataInterface $value, array $format = [])
  {
    $item = $value->getString();
    parent::__construct($item, $format);
  }
}
