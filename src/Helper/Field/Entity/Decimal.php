<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Entity;

use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use RtLopez\Decimal as BigNumber;

/**
 * Class Decimal
 */
class Decimal extends FloatNumber
{
  /**
   * @param TypedDataInterface $value
   * @param array              $format
   */
  function __construct(TypedDataInterface $value, array $format = [])
  {
    $item = $value->getString();
    parent::__construct(BigNumber::create($item), $format);
  }
}
