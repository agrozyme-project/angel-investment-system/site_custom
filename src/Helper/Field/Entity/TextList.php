<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Entity;

use Drupal\site_custom\Helper\Field\Base\EntityList;

/**
 * Class TextList
 */
class TextList extends EntityList
{
  protected $default = '';
}
