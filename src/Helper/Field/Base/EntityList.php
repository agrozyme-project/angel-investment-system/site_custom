<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Base;

/**
 * Class EntityList
 */
abstract class EntityList extends EntityField
{
  protected $default = null;

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   *
   * @param array $format
   *
   * @return string
   */
  function toString(array $format = []): string
  {
    $value = $this->getItem();
    return strval($value);
  }

  /**
   * @return string
   */
  function getIndex(): string
  {
    return $this->value->getString();
  }

  /**
   * @return mixed
   */
  function getItem()
  {
    $index = $this->getIndex();
    $items = $this->getAllowedValues();
    return $items[$index] ?? $this->default;
  }

  /**
   * @return array
   */
  function getAllowedValues(): array
  {
    return $this->value->getDataDefinition()->getSetting('allowed_values');
  }
}
