<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Base;

/**
 * Class FieldBase
 */
abstract class FieldBase
{
  protected $value;
  protected $format = [];

  /**
   * @param mixed $value
   * @param array $format
   */
  function __construct($value, array $format = [])
  {
    $this->value = $value;
    $this->format = $this->mergeFormat($format);
  }

  //  /**
  //   * @param mixed $value
  //   * @param array $format
  //   *
  //   * @return static
  //   */
  //  static function create($value, array $format = [])
  //  {
  //    return new static($value, $format);
  //  }

  /**
   * @return mixed
   */
  function getValue()
  {
    return $this->value;
  }

  /**
   * @param array $format
   *
   * @return string
   */
  function toString(array $format = []): string
  {
    return strval($this->value);
  }

  /**
   * @param array $format
   *
   * @return array
   */
  function mergeFormat(array $format): array
  {
    return array_merge($this->format, $format);
  }
}
