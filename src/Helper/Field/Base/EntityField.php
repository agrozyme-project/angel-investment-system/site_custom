<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Base;

use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Class EntityField
 */
class EntityField extends FieldBase
{
  /**
   * @var TypedDataInterface $value
   */
  protected $value;

  /**
   * @param TypedDataInterface $value
   * @param array              $format
   */
  function __construct(TypedDataInterface $value, array $format = [])
  {
    parent::__construct($value, $format);
  }
}
