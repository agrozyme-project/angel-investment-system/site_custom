<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Value;

use Drupal\site_custom\Helper\Field\Base\FieldBase;
use RtLopez\Decimal;

/**
 * Class FloatNumber
 */
class FloatNumber extends FieldBase
{
  /**
   * @var Decimal $value
   */
  protected $value;

  protected $format = [
    'precision' => 2,
    'decimal_point' => '.',
    'thousands_separator' => ','
  ];

  /**
   * @param number $value
   * @param array  $format
   */
  function __construct($value, array $format = [])
  {
    parent::__construct(Decimal::create($value), $format);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * @inheritDoc
   */
  function toString(array $format = []): string
  {
    $options = $this->mergeFormat($format);
    $value = floatval($this->value->__toString());

    return number_format($value, $options['precision'], $options['decimal_point'], $options['thousands_separator']);
  }

  /**
   * @return Decimal
   */
  function getValue(): Decimal
  {
    return parent::getValue();
  }
}
