<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Value;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\site_custom\Helper\Field\Base\FieldBase;

/**
 * Class DateTime
 */
class DateTime extends FieldBase
{
  /**
   * @var DrupalDateTime $value
   */
  protected $value;
  protected $format = [
    'format' => DateTimeItemInterface::DATETIME_STORAGE_FORMAT
  ];

  /**
   * @param DrupalDateTime $value
   * @param array          $format
   */
  function __construct(DrupalDateTime $value, array $format = [])
  {
    parent::__construct($value, $format);
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * @inheritDoc
   */
  function toString(array $format = []): string
  {
    $options = $this->mergeFormat($format);
    return $this->value->format($options['format']);
  }

  /**
   * @return DrupalDateTime
   */
  function getValue(): DrupalDateTime
  {
    return parent::getValue();
  }
}
