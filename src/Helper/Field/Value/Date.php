<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Value;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Class Date
 */
class Date extends DateTime
{
  protected $format = [
    'format' => DateTimeItemInterface::DATE_STORAGE_FORMAT
  ];
}
