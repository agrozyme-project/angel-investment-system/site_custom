<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Value;

/**
 * Class Integer
 */
class IntegerNumber extends FloatNumber
{
  protected $format = [
    'precision' => 0,
    'decimal_point' => '.',
    'thousands_separator' => ','
  ];
}
