<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Value;

use Drupal\site_custom\Helper\Field\Base\FieldBase;

/**
 * Class Text
 */
class Text extends FieldBase
{
  /**
   * @param string $value
   * @param array  $format
   */
  function __construct(string $value, array $format = [])
  {
    parent::__construct($value, $format);
  }
}
