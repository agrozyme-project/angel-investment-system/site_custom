<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Report\Base\ReportBase;

/**
 * Class ReportFormBase
 */
abstract class ReportFormBase extends FormBase
{
  /**
   * @inheritDoc
   */
  function buildForm(array $form, FormStateInterface $form_state)
  {
    $filter = $this->getFilter();
    $actions = $this->buildActions($form, $form_state);
    $form['filters'] = $filter->build() + $actions;
    $form['actions'] = $actions;
    $form['content'] = $this->getReport($filter)->getContent();
    return $form;
  }

  /**
   * @return Url
   */
  function getUrl(): Url
  {
    $route = $this->getRouteMatch()->getRouteName();
    return Url::fromRoute($route);
  }

  /**
   * @inheritDoc
   */
  function submitForm(array &$form, FormStateInterface $form_state)
  {
  }

  /**
   * @param array              $form
   * @param FormStateInterface $form_state
   */
  function submitFormApply(array &$form, FormStateInterface $form_state)
  {
    $form_state->cleanValues();
    $url = $this->getUrl();
    $url->setOption('query', $form_state->getValues());
    $form_state->setRedirectUrl($url);
  }

  /**
   * @param array              $form
   * @param FormStateInterface $form_state
   */
  function submitFormReset(array &$form, FormStateInterface $form_state)
  {
    $form_state->cleanValues();
    $url = $this->getUrl();
    $form_state->setRedirectUrl($url);
  }

  /**
   * @param array              $form
   * @param FormStateInterface $form_state
   *
   * @return array
   */
  function buildActions(array &$form, FormStateInterface $form_state)
  {
    $items = ['#type' => 'actions'];
    $items['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#submit' => [[$this, 'submitFormApply']]
    ];
    $items['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => [[$this, 'submitFormReset']]
    ];
    return $items;
  }

  /**
   * @return FormFilterBase
   */
  abstract protected function getFilter(): FormFilterBase;

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  abstract protected function getReport($filter): ReportBase;
}
