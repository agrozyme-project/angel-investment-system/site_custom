<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\OperationDetail;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Filter\Date;
use Drupal\site_custom\Filter\InvestmentTarget;
use Tightenco\Collect\Support\Collection;

/**
 * Class OperationDetailFilter
 */
class OperationDetailFilter extends FormFilterBase
{
  /**
   * @return Collection
   */
  protected function setupItems(): Collection
  {
    $items = [
      'date' => new Date('date'),
      'investment_target' => new InvestmentTarget('investment_target')
    ];

    return Collection::make($items);
  }
}
