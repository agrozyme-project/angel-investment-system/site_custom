<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\OperationDetail;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\Base\ReportNull;

/**
 * Class OperationDetailForm
 */
class OperationDetailForm extends ReportFormBase
{
  /**
   * @inheritDoc
   */
  function getFormId()
  {
    return 'OperationDetailForm';
  }

  /**
   * @return FormFilterBase
   */
  protected function getFilter(): FormFilterBase
  {
    return new OperationDetailFilter();
  }

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  protected function getReport($filter): ReportBase
  {
    return new ReportNull($filter);
  }
}
