<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentGainSummary;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\InvestmentGainSummary\InvestmentGainSummaryReport;

/**
 * Class InvestmentGainSummaryForm
 */
class InvestmentGainSummaryForm extends ReportFormBase
{
  /**
   * @inheritDoc
   */
  function getFormId()
  {
    return 'InvestmentGainSummaryForm';
  }

  /**
   * @return FormFilterBase
   */
  protected function getFilter(): FormFilterBase
  {
    return new InvestmentGainSummaryFormFilter();
  }

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  protected function getReport($filter): ReportBase
  {
    return new InvestmentGainSummaryReport($filter);
  }
}
