<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentGainSummary;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Filter\Date;
use Drupal\site_custom\Filter\InvestmentTargets;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentGainSummaryFormFilter
 */
class InvestmentGainSummaryFormFilter extends FormFilterBase
{
  /**
   * @return Collection
   */
  protected function setupItems(): Collection
  {
    $items = [
      'date' => new Date('date'),
      'investment_targets' => new InvestmentTargets('investment_targets')
    ];

    return Collection::make($items);
  }
}
