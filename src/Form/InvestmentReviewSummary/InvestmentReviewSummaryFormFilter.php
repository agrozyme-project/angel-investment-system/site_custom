<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentReviewSummary;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Filter\Date;
use Drupal\site_custom\Filter\InvestmentTargets;
use Drupal\site_custom\Filter\InvestmentTypes;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentReviewSummaryFormFilter
 */
class InvestmentReviewSummaryFormFilter extends FormFilterBase
{
  /**
   * @inheritDoc
   */
  protected function setupItems(): Collection
  {
    $items = [
      'date' => new Date('date'),
      'investment_types' => new InvestmentTypes('investment_types'),
      'investment_targets' => new InvestmentTargets('investment_targets')
    ];

    return Collection::make($items);
  }
}
