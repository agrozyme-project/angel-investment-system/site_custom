<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentReviewSummary;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\InvestmentReviewSummary\InvestmentReviewSummaryReport;

/**
 * Class InvestmentReviewSummaryForm
 */
class InvestmentReviewSummaryForm extends ReportFormBase
{
  /**
   * @inheritDoc
   */
  function getFormId()
  {
    return 'InvestmentReviewSummaryForm';
  }

  /**
   * @inheritDoc
   */
  protected function getFilter(): FormFilterBase
  {
    return new InvestmentReviewSummaryFormFilter();
  }

  /**
   * @inheritDoc
   */
  protected function getReport($filter): ReportBase
  {
    return new InvestmentReviewSummaryReport($filter);
  }
}
