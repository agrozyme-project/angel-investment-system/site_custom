<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\FunderInvestmentLogSummary;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\FunderInvestmentLogSummary\FunderInvestmentLogSummaryReport;

/**
 * Class FunderInvestmentLogSummaryForm
 */
class FunderInvestmentLogSummaryForm extends ReportFormBase
{
  /**
   * @inheritDoc
   */
  function getFormId()
  {
    return 'FunderInvestmentLogSummaryForm';
  }

  /**
   * @return FormFilterBase
   */
  protected function getFilter(): FormFilterBase
  {
    return new FunderInvestmentLogSummaryFormFilter();
  }

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  protected function getReport($filter): ReportBase
  {
    return new FunderInvestmentLogSummaryReport($filter);
  }
}
