<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentAnalysis;

use Drupal\site_custom\Filter\AnalysisType;
use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Filter\Date;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentAnalysisFormFilter
 */
class InvestmentAnalysisFormFilter extends FormFilterBase
{
  /**
   * @return Collection
   */
  protected function setupItems(): Collection
  {
    $items = [
      'date' => new Date('date'),
      'analysis_type' => new AnalysisType('analysis_type')
    ];

    return Collection::make($items);
  }
}
