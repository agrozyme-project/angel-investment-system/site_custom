<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentAnalysis;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\InvestmentAnalysis\InvestmentAnalysisReport;

/**
 * Class InvestmentAnalysisForm
 */
class InvestmentAnalysisForm extends ReportFormBase
{
  /**
   * @inheritDoc
   */
  function getFormId()
  {
    return 'InvestmentAnalysisForm';
  }

  /**
   * @return FormFilterBase
   */
  protected function getFilter(): FormFilterBase
  {
    return new InvestmentAnalysisFormFilter();
  }

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  protected function getReport($filter): ReportBase
  {
    return new InvestmentAnalysisReport($filter);
  }
}
