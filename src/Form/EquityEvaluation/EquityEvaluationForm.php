<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\EquityEvaluation;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\EquityEvaluation\EquityEvaluationReport;

/**
 * Class EquityEvaluationForm
 */
class EquityEvaluationForm extends ReportFormBase
{
  /**
   * @inheritDoc
   */
  function getFormId()
  {
    return 'EquityEvaluationForm';
  }

  /**
   * @return FormFilterBase
   */
  protected function getFilter(): FormFilterBase
  {
    return new EquityEvaluationFormFilter();
  }

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  protected function getReport($filter): ReportBase
  {
    return new EquityEvaluationReport($filter);
  }
}
