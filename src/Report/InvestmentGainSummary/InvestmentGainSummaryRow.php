<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentGainSummary;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Base\FieldBase;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\RowField\ArrayPropertyFactory;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use Drupal\site_custom\Report\RowField\Node\Company;
use Drupal\site_custom\Report\RowField\Node\FunderShareholdingQuantity;
use Drupal\site_custom\Report\RowField\Node\InvestmentGain;

/**
 * Class InvestmentGainSummary
 */
class InvestmentGainSummaryRow extends RowBase
{
  /**
   * @param EntityInterface $investmentTarget
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $investmentTarget, DrupalDateTime $date)
  {
    $this->dataSource = new InvestmentGainSummaryDataSource($investmentTarget, $date);

    $this->fields = $this->setupFields();
  }

  /**
   * @return FieldBase[]
   */
  protected function setupFields(): array
  {
    $item = $this->dataSource;
    $entity = EntityFieldFactory::create();
    $array = ArrayPropertyFactory::create();

    return [
      'Company' => Company::create($item->getInvestmentTarget(), $item->getLastInvestmentProfile()),
      'Investors' => $entity->get($item->getInvestmentTarget(), 'investors'),
      'Industry Type' => $entity->get($item->getLastInvestmentProfile(), 'industry_type'),
      'Investment Amount' => $array->get($item->getSumInvestmentLogs(), 'funder_investment_amount'),
      'Shareholding Quantity' => FunderShareholdingQuantity::create($item->getSumInvestmentLogs()),
      'Date' => $entity->get($item->getLastInvestmentLog(), 'date'),
      'Funder Cash Dividend' => $array->get($item->getSumInvestmentLogs(), 'funder_cash_dividend'),
      'Funder Dispose Share' => FunderShareholdingQuantity::create($item->getSumDispositionLogs()),
      'Investment Gain' => InvestmentGain::create($item->getLastFinancialSummary(), $item->getSumInvestmentLogs())
    ];
  }
}
