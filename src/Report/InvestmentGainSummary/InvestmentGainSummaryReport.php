<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentGainSummary;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\site_custom\Form\InvestmentGainSummary\InvestmentGainSummaryFormFilter;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\Base\TableReportBase;

/**
 * Class EquityEvaluationReport
 */
class InvestmentGainSummaryReport extends TableReportBase
{
  /**
   * @param InvestmentGainSummaryFormFilter $filter
   */
  function __construct(InvestmentGainSummaryFormFilter $filter)
  {
    parent::__construct($filter);
  }

  /**
   * @return TranslatableMarkup[]
   */
  function getTableHeader(): array
  {
    $options = ['context' => 'InvestmentGainSummaryReport'];

    return [
      '#' => t('#', [], $options),
      'Company' => t('Company', [], $options),

      'Investors' => t('Investors', [], $options),
      'Industry Type' => t('Industry Type', [], $options),

      'Investment Amount' => t('Investment Amount', [], $options),
      'Shareholding Quantity' => t('Shareholding Quantity', [], $options),

      'Date' => t('Date', [], $options),
      'Funder Cash Dividend' => t('Funder Cash Dividend', [], $options),
      'Funder Dispose Share' => t('Funder Dispose Share', [], $options),
      'Investment Gain' => t('Investment Gain', [], $options)
    ];
  }

  /**
   * @return array
   */
  function getTableOptions(): array
  {
    $data = $this->filter
      ->getItems()
      ->get('investment_targets')
      ->getItem();

    return $this->wrapperTableOptions($data);
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  protected function getTableRow($item): RowBase
  {
    $items = $this->filter->getItems();
    return new InvestmentGainSummaryRow($item, $items->get('date')->getItem());
  }
}
