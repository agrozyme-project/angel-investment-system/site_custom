<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentAnalysis;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\site_custom\Filter\AnalysisType;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\Base\RowNull;
use Drupal\site_custom\Report\Base\TableReportBase;

/**
 * @package Drupal\site_custom\Report\InvestmentAnalysis
 */
class InvestmentAnalysisReport extends TableReportBase
{
  /**
   * @return TranslatableMarkup[]
   */
  function getTableHeader(): array
  {
    $options = ['context' => 'InvestmentAnalysisReport'];

    /** @var AnalysisType $analysisType */
    $analysisType = $this->filter->getItems()->get('analysis_type');
    $label = $analysisType->getLabel();

    return [
      '#' => t('#', [], $options),
      'Company Count' => $label,
      'Companys' => t('Companys', [], $options),
      'Paid In Capital' => t('Paid In Capital', [], $options),
      'Funder Shareholding Ratio' => t('Funder Shareholding Ratio', [], $options),
      'Review Amount' => t('Review Amount', [], $options),
      'Funder Grant Amount' => t('Funder Grant Amount', [], $options),
      'Investor Grant Amount' => t('Investor Grant Amount', [], $options),
      'Investor Investment Amount' => t('Investor Investment Amount', [], $options)
    ];
  }

  /**
   * @return array
   */
  function getTableOptions(): array
  {
    return [];
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  protected function getTableRow($item): RowBase
  {
    return new RowNull();
  }
}
