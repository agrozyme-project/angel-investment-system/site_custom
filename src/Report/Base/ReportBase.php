<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

use Drupal\site_custom\Filter\Base\FormFilterBase;

/**
 * Class ReportBase
 */
abstract class ReportBase
{
  protected $filter;

  /**
   * @param FormFilterBase $filter
   */
  function __construct(FormFilterBase $filter)
  {
    $this->filter = $filter;
  }

  /**
   * @return array
   */
  abstract function getContent(): array;
}
