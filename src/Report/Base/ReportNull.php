<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

/**
 * Class ReportNull
 */
class ReportNull extends ReportBase
{
  /**
   * @return array
   */
  function getContent(): array
  {
    return [];
  }
}
