<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use RtLopez\Decimal;
use Tightenco\Collect\Support\Collection;

/**
 * Class TableReportBase
 *
 */
abstract class TableReportBase extends ReportBase
{
  protected $rows = [];

  /**
   * @return array
   */
  function getContent(): array
  {
    $header = $this->getTableHeader();
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $this->getTableRows(array_keys($header))
    ];
  }

  /**
   * @param array $keys
   *
   * @return array
   */
  function getTableRows(array $keys)
  {
    $rows = [];
    $options = $this->getTableOptions();

    foreach ($options as $option) {
      $row = [];

      foreach ($keys as $key) {
        $row[] = isset($option[$key]) ? $option[$key] : '';
      }

      $rows[] = $row;
    }

    return $rows;
  }

  /**
   * @return TranslatableMarkup[]
   */
  abstract function getTableHeader(): array;

  /**
   * @return array
   */
  abstract function getTableOptions(): array;

  /**
   * @param Collection $items
   *
   * @return array
   */
  protected function wrapperTableOptions(Collection $items): array
  {
    $rows = [];
    $data = [];
    $count = Decimal::create(0);

    $items->each(function ($item, $key) use (&$rows, &$data, &$count) {
      $row = $this->getTableRow($item);
      $rows[] = $row;
      $data[] = $this->wrapperTableRow($row, $key, $count);
    });

    $this->rows = $rows;
    return $data;
  }

  /**
   * @param RowBase $row
   * @param         $key
   * @param Decimal $count
   *
   * @return string[]
   */
  protected function wrapperTableRow(RowBase $row, $key, Decimal &$count): array
  {
    $count = $count->add(1);
    $data = $row->formatFields();
    $data['#'] = $count->toString();
    return $data;
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  abstract protected function getTableRow($item): RowBase;
}
