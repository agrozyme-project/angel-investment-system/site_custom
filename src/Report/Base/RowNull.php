<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

use Drupal\site_custom\Helper\Field\Base\FieldBase;

/**
 * Class RowNull
 */
class RowNull extends RowBase
{
  /**
   * @return FieldBase[]
   */
  protected function setupFields(): array
  {
    return [];
  }
}
