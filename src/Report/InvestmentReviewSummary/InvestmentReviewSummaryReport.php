<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentReviewSummary;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\NodeInterface;
use Drupal\site_custom\Form\InvestmentReviewSummary\InvestmentReviewSummaryFormFilter;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\Base\TableReportBase;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentReviewSummaryReport
 */
class InvestmentReviewSummaryReport extends TableReportBase
{
  /**
   * @param InvestmentReviewSummaryFormFilter $filter
   */
  function __construct(InvestmentReviewSummaryFormFilter $filter)
  {
    parent::__construct($filter);
  }

  /**
   * @return TranslatableMarkup[]
   */
  function getTableHeader(): array
  {
    $options = ['context' => 'InvestmentReviewSummaryReport'];

    return [
      '#' => t('#', [], $options),
      'Company' => t('Company', [], $options),
      'Review Date' => t('Review Date', [], $options),
      //      'Grant Date' => t('Grant Date', [], $options),
      'Investment Type' => t('Investment Type', [], $options),
      'Investment Amount' => t('Investment Amount', [], $options),
      'Funder Shareholding Quantity' => t('Funder Shareholding Quantity', [], $options),
      'Funder Shareholding Ratio' => t('Funder Shareholding Ratio', [], $options),
      'Cost Per Share' => t('Cost Per Share', [], $options),
      'Investor Investment Amount' => t('Investor Investment Amount', [], $options),
      'Investor Shareholding Quantity' => t('Investor Shareholding Quantity', [], $options)
      //      'Investor Shareholding Ratio' => t('Investor Shareholding Ratio', [], $options)
      //      'Funder Seat' => t('Funder Seat', [], $options),
      //      'Net Operating Revenue' => t('Net Operating Revenue', [], $options),
      //      'Net Income' => t('Net Income', [], $options),
      //      'Earning Per Share' => t('Earning Per Share', [], $options),
      //      'Book Value Per Share' => t('Book Value Per Share', [], $options)
    ];
  }

  /**
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getTableOptions(): array
  {
    /** @var Collection $data */
    $data = $this->filter
      ->getItems()
      ->get('investment_targets')
      ->getItem();

    $data = $this->filterRow($data);
    return $this->wrapperTableOptions($data);
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  protected function getTableRow($item): RowBase
  {
    $items = $this->filter->getItems();
    return new InvestmentReviewSummaryRow($item, $items->get('date')->getItem());
  }

  /**
   * @param Collection $items
   *
   * @return Collection
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function filterRow(Collection $items)
  {
    $investment_types = $this->filter
      ->getItems()
      ->get('investment_types')
      ->getItem()
      ->keys()
      ->toArray();

    if (empty($investment_types)) {
      return $items;
    }

    $investment_targets = $items->keys()->toArray();
    /** @var DrupalDateTime $date */
    $date = $this->filter
      ->getItems()
      ->get('date')
      ->getItem();

    $storage = Drupal::entityTypeManager()->getStorage('node');

    $ids = Drupal::entityQueryAggregate('node')
      ->groupBy('investment_target')
      ->condition('type', 'investment_review')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('investment_target', $investment_targets, 'IN')
      ->condition('investment_type', $investment_types, 'IN')
      ->condition('review_date', $date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), '<=')
      ->execute();

    return Collection::make($ids)->map(function ($value, $key) use ($items) {
      $id = $value['investment_target_target_id'];
      return $items[$id];
    });
  }
}
