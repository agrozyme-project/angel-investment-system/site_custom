<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentReviewSummary;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Entity\Node;
use Drupal\site_custom\Helper\InvestmentReason;
use Drupal\site_custom\Report\DataSource\FinancialSummary;
use Drupal\site_custom\Report\DataSource\InvestmentLog;
use Drupal\site_custom\Report\DataSource\InvestmentProfile;
use Drupal\site_custom\Report\DataSource\InvestmentReview;
use RtLopez\Decimal;

/**
 * Class InvestmentReviewSummaryDataSource
 */
class InvestmentReviewSummaryDataSource
{
  protected $investmentTarget;

  protected $date;

  protected $investmentLogs = [];

  protected $sumInvestmentLogs = [];

  protected $lastInvestmentLog;

  protected $lastInvestmentProfile;

  protected $financialSummary;

  protected $investmentReview;

  /**
   * @param EntityInterface $investmentTarget
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $investmentTarget, DrupalDateTime $date)
  {
    $this->investmentTarget = $investmentTarget;
    $this->date = $date;
    $this->investmentLogs = $this->loadInvestmentLogs();
    $this->sumInvestmentLogs = InvestmentReason::sum($this->investmentLogs);
    $this->lastInvestmentLog = $this->loadLastInvestmentLog();
    $this->lastInvestmentProfile = $this->loadLastInvestmentProfile();
    $this->financialSummary = $this->LoadFinancialSummary();
    $this->investmentReview = $this->LoadInvestmentReview();
  }

  /**
   * @return EntityInterface
   */
  function getInvestmentTarget(): EntityInterface
  {
    return $this->investmentTarget;
  }

  /**
   * @return DrupalDateTime
   */
  function getDate(): DrupalDateTime
  {
    return $this->date;
  }

  /**
   * @return EntityInterface[]
   */
  function getInvestmentLogs(): array
  {
    return $this->investmentLogs;
  }

  /**
   * @return Decimal[]
   */
  function getSumInvestmentLogs(): array
  {
    return $this->sumInvestmentLogs;
  }

  /**
   * @return EntityInterface
   */
  function getLastInvestmentLog(): EntityInterface
  {
    return $this->lastInvestmentLog;
  }

  /**
   * @return EntityInterface
   */
  function getLastInvestmentProfile(): EntityInterface
  {
    return $this->lastInvestmentProfile;
  }

  /**
   * @return EntityInterface
   */
  function getFinancialSummary(): EntityInterface
  {
    return $this->financialSummary;
  }

  /**
   * @return mixed
   */
  public function getInvestmentReview()
  {
    return $this->investmentReview;
  }

  /**
   * @return EntityInterface[]
   */
  protected function loadInvestmentLogs(): array
  {
    $item = new InvestmentLog($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastInvestmentLog(): EntityInterface
  {
    $items = $this->investmentLogs;

    return empty($items) ? Node\InvestmentLog::create()->getEmptyEntity() : reset($items);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastInvestmentProfile(): EntityInterface
  {
    $item = new InvestmentProfile($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function LoadFinancialSummary(): EntityInterface
  {
    $item = new FinancialSummary($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function LoadInvestmentReview(): EntityInterface
  {
    $item = new InvestmentReview($this->investmentTarget);
    return $item->byDate($this->date);
  }
}
