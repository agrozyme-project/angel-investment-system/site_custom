<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentReviewSummary;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\RowField\ArrayPropertyFactory;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use Drupal\site_custom\Report\RowField\Node\Company;
use Drupal\site_custom\Report\RowField\Node\CostPerShare;
use Drupal\site_custom\Report\RowField\Node\FunderShareholdingQuantity;
use Drupal\site_custom\Report\RowField\Node\FunderShareholdingRatio;
use Drupal\site_custom\Report\RowField\Node\InvestorShareholdingQuantity;
use Drupal\site_custom\Report\RowField\Node\InvestorShareholdingRatio;

/**
 * Class InvestmentReviewSummaryRow
 */
class InvestmentReviewSummaryRow extends RowBase
{
  /**
   * @param EntityInterface $investmentTarget
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $investmentTarget, DrupalDateTime $date)
  {
    $this->dataSource = new InvestmentReviewSummaryDataSource($investmentTarget, $date);

    $this->fields = $this->setupFields();
  }

  /**
   * @inheritDoc
   */
  protected function setupFields(): array
  {
    $item = $this->dataSource;
    $entity = EntityFieldFactory::create();
    $array = ArrayPropertyFactory::create();

    return [
      'Company' => Company::create($item->getInvestmentTarget(), $item->getLastInvestmentProfile()),
      'Review Date' => $entity->get($item->getInvestmentReview(), 'review_date'),
      'Grant Date' => $entity->get($item->getInvestmentReview(), 'grant_date'),
      'Investment Type' => $entity->get($item->getInvestmentReview(), 'investment_type'),
      'Investment Amount' => $array->get($item->getSumInvestmentLogs(), 'funder_investment_amount'),
      'Funder Shareholding Quantity' => FunderShareholdingQuantity::create($item->getSumInvestmentLogs()),
      'Funder Shareholding Ratio' => FunderShareholdingRatio::create(
        $item->getLastInvestmentLog(),
        $item->getSumInvestmentLogs()
      ),
      'Cost Per Share' => CostPerShare::create($item->getSumInvestmentLogs()),
      'Investor Investment Amount' => $array->get($item->getSumInvestmentLogs(), 'investor_investment_amount'),
      'Investor Shareholding Quantity' => InvestorShareholdingQuantity::create($item->getSumInvestmentLogs()),
      'Investor Shareholding Ratio' => InvestorShareholdingRatio::create(
        $item->getLastInvestmentLog(),
        $item->getSumInvestmentLogs()
      ),
      'Funder Seat' => $entity->get($item->getLastInvestmentProfile(), 'funder_seat'),
      'Net Operating Revenue' => $entity->get($item->getFinancialSummary(), 'net_operating_revenue'),
      'Net Income' => $entity->get($item->getFinancialSummary(), 'net_income'),
      'Earning Per Share' => $entity->get($item->getFinancialSummary(), 'earning_per_share'),
      'Book Value Per Share' => $entity->get($item->getFinancialSummary(), 'book_value_per_share')
    ];
  }
}
