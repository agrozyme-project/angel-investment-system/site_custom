<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\FunderInvestmentLogSummary;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Base\FieldBase;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\RowField\ArrayPropertyFactory;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use Drupal\site_custom\Report\RowField\Node\Company;
use Drupal\site_custom\Report\RowField\Node\CostPerShare;
use Drupal\site_custom\Report\RowField\Node\EstimateGain;
use Drupal\site_custom\Report\RowField\Node\EvaluationValue;
use Drupal\site_custom\Report\RowField\Node\FunderShareholdingQuantity;
use Drupal\site_custom\Report\RowField\Node\RealizedGain;

/**
 * Class FunderInvestmentLogSummaryRow
 */
class FunderInvestmentLogSummaryRow extends RowBase
{
  /**
   * EquityEvaluationRow constructor.
   *
   * @param EntityInterface $target
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $target, DrupalDateTime $date)
  {
    $this->dataSource = new FunderInvestmentLogSummaryDataSource($target, $date);

    $this->fields = $this->setupFields();
  }

  /**
   * @return FieldBase[]
   */
  protected function setupFields(): array
  {
    $item = $this->dataSource;
    $entity = EntityFieldFactory::create();
    $array = ArrayPropertyFactory::create();

    return [
      'Company' => Company::create($item->getInvestmentTarget(), $item->getLastInvestmentProfile()),
      'Review Date' => $entity->get($item->getLastInvestmentReview(), 'review_date'),
      'Investment Amount' => $array->get($item->getSumInvestmentLogs(), 'funder_investment_amount'),
      'Shareholding Quantity' => FunderShareholdingQuantity::create($item->getSumInvestmentLogs()),
      'Cost Per Share' => CostPerShare::create($item->getSumInvestmentLogs()),
      'Book Value Per Share' => $entity->get($item->getLastFinancialSummary(), 'book_value_per_share'),
      'Interest Income' => $array->get($item->getSumInvestmentLogs(), 'funder_interest_income'),
      'Cash Dividend' => $array->get($item->getSumInvestmentLogs(), 'funder_cash_dividend'),
      'Realized Gain' => RealizedGain::create($item->getSumInvestmentLogs()),
      'Investment Baseline' => $entity->get($item->getLastFinancialSummary(), 'investment_baseline'),
      'Market Value Per Share' => $entity->get($item->getLastFinancialSummary(), 'market_value_per_share'),
      'Evaluation Value' => EvaluationValue::create($item->getLastFinancialSummary(), $item->getSumInvestmentLogs()),
      'Estimate Gain' => EstimateGain::create($item->getLastFinancialSummary(), $item->getSumInvestmentLogs())
    ];
  }
}
