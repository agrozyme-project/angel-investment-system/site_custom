<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\FunderInvestmentLogSummary;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\site_custom\Form\FunderInvestmentLogSummary\FunderInvestmentLogSummaryFormFilter;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\Base\TableReportBase;

/**
 * Class FunderInvestmentLogSummaryReport
 */
class FunderInvestmentLogSummaryReport extends TableReportBase
{
  protected $date;

  protected $targets;

  /**
   * @param FunderInvestmentLogSummaryFormFilter $filter
   */
  function __construct(FunderInvestmentLogSummaryFormFilter $filter)
  {
    parent::__construct($filter);
  }

  /**
   * @return TranslatableMarkup[]
   */
  function getTableHeader(): array
  {
    $options = ['context' => 'FunderInvestmentLogSummary'];

    return [
      '#' => t('#', [], $options),
      'Company' => t('Company', [], $options),
      'Review Date' => t('Review Date', [], $options),
      'Investment Amount' => t('Investment Amount', [], $options),
      'Shareholding Quantity' => t('Shareholding Quantity', [], $options),
      'Cost Per Share' => t('Cost Per Share', [], $options),
      'Book Value Per Share' => t('Book Value Per Share', [], $options),
      //      'Interest Income' => t('Interest Income', [], $options),
      'Cash Dividend' => t('Cash Dividend', [], $options),

      'Realized Gain' => t('Realized Gain', [], $options),
      'Investment Baseline' => t('Investment Baseline', [], $options),
      'Market Value Per Share' => t('Market Value Per Share', [], $options),
      'Evaluation Value' => t('Evaluation Value', [], $options),
      'Estimate Gain' => t('Estimate Gain', [], $options)
    ];
  }

  /**
   * @return array
   */
  function getTableOptions(): array
  {
    $data = $this->filter
      ->getItems()
      ->get('investment_targets')
      ->getItem();

    return $this->wrapperTableOptions($data);
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  protected function getTableRow($item): RowBase
  {
    $items = $this->filter->getItems();
    return new FunderInvestmentLogSummaryRow($item, $items->get('date')->getItem());
  }
}
