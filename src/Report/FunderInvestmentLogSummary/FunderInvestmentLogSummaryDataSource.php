<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\FunderInvestmentLogSummary;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\InvestmentReason;
use Drupal\site_custom\Report\DataSource\FinancialSummary;
use Drupal\site_custom\Report\DataSource\InvestmentLog;
use Drupal\site_custom\Report\DataSource\InvestmentProfile;
use Drupal\site_custom\Report\DataSource\InvestmentReview;
use RtLopez\Decimal;

/**
 * Class FunderInvestmentLogSummaryDataSource
 */
class FunderInvestmentLogSummaryDataSource
{
  protected $investmentTarget;

  protected $date;

  protected $investmentLogs = [];

  protected $sumInvestmentLogs = [];

  protected $lastInvestmentReview;

  protected $lastFinancialSummary;

  protected $lastInvestmentProfile;

  /**
   * @param EntityInterface $investmentTarget
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $investmentTarget, DrupalDateTime $date)
  {
    $this->investmentTarget = $investmentTarget;
    $this->date = $date;
    $this->lastInvestmentReview = $this->loadLastInvestmentReview();
    $this->investmentLogs = $this->loadInvestmentLogs();
    $this->sumInvestmentLogs = InvestmentReason::sum($this->investmentLogs);
    $this->lastFinancialSummary = $this->loadLastFinancialSummary();
    $this->lastInvestmentProfile = $this->loadLastInvestmentProfile();
  }

  /**
   * @return EntityInterface
   */
  function getInvestmentTarget(): EntityInterface
  {
    return $this->investmentTarget;
  }

  /**
   * @return DrupalDateTime
   */
  function getDate(): DrupalDateTime
  {
    return $this->date;
  }

  /**
   * @return EntityInterface[]
   */
  function getInvestmentLogs(): array
  {
    return $this->investmentLogs;
  }

  /**
   * @return Decimal[]
   */
  function getSumInvestmentLogs(): array
  {
    return $this->sumInvestmentLogs;
  }

  /**
   * @return EntityInterface
   */
  function getLastInvestmentReview(): EntityInterface
  {
    return $this->lastInvestmentReview;
  }

  /**
   * @return EntityInterface
   */
  function getLastFinancialSummary(): EntityInterface
  {
    return $this->lastFinancialSummary;
  }

  /**
   * @return EntityInterface
   */
  function getLastInvestmentProfile(): EntityInterface
  {
    return $this->lastInvestmentProfile;
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastInvestmentReview()
  {
    $item = new InvestmentReview($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface[]
   */
  protected function loadInvestmentLogs(): array
  {
    $item = new InvestmentLog($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastFinancialSummary()
  {
    $item = new FinancialSummary($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastInvestmentProfile()
  {
    $item = new InvestmentProfile($this->investmentTarget);
    return $item->byDate($this->date);
  }
}
