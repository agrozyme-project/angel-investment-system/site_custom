<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\EquityEvaluation;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\site_custom\Form\EquityEvaluation\EquityEvaluationFormFilter;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\Base\TableReportBase;

/**
 * Class EquityEvaluationReport
 */
class EquityEvaluationReport extends TableReportBase
{
  /**
   * @param EquityEvaluationFormFilter $filter
   */
  function __construct(EquityEvaluationFormFilter $filter)
  {
    parent::__construct($filter);
  }

  /**
   * @return TranslatableMarkup[]
   */
  function getTableHeader(): array
  {
    $options = ['context' => 'EquityEvaluationReport'];

    return [
      '#' => t('#', [], $options),
      'Company' => t('Company', [], $options),
      'Shareholding Ratio' => t('Shareholding Ratio', [], $options),
      'Shareholding Quantity' => t('Shareholding Quantity', [], $options),
      'Investment Amount' => t('Investment Amount', [], $options),
      'Cost Per Share' => t('Cost Per Share', [], $options),
      'Book Value Per Share' => t('Book Value Per Share', [], $options),
      'Market Value Per Share' => t('Market Value Per Share', [], $options),
      'Investment Baseline' => t('Investment Baseline', [], $options),
      'Investment Gain' => t('Investment Gain', [], $options),
      'Capital Market Status' => t('Capital Market Status', [], $options)
    ];
  }

  /**
   * @return array
   */
  function getTableOptions(): array
  {
    $data = $this->filter
      ->getItems()
      ->get('investment_targets')
      ->getItem();

    return $this->wrapperTableOptions($data);
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  protected function getTableRow($item): RowBase
  {
    $items = $this->filter->getItems();
    return new EquityEvaluationRow($item, $items->get('date')->getItem());
  }
}
