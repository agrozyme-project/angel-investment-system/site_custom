<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\EquityEvaluation;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Base\FieldBase;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\RowField\ArrayPropertyFactory;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use Drupal\site_custom\Report\RowField\Node\Company;
use Drupal\site_custom\Report\RowField\Node\CostPerShare;
use Drupal\site_custom\Report\RowField\Node\FunderShareholdingQuantity;
use Drupal\site_custom\Report\RowField\Node\FunderShareholdingRatio;
use Drupal\site_custom\Report\RowField\Node\InvestmentGain;

/**
 * Class EquityEvaluationRow
 */
class EquityEvaluationRow extends RowBase
{
  /**
   * EquityEvaluationRow constructor.
   *
   * @param EntityInterface $investmentTarget
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $investmentTarget, DrupalDateTime $date)
  {
    $this->dataSource = new EquityEvaluationDataSource($investmentTarget, $date);

    $this->fields = $this->setupFields();
  }

  /**
   * @return FieldBase[]
   */
  protected function setupFields(): array
  {
    $item = $this->dataSource;
    $entity = EntityFieldFactory::create();
    $array = ArrayPropertyFactory::create();

    return [
      'Company' => Company::create($item->getInvestmentTarget(), $item->getLastInvestmentProfile()),
      'Shareholding Ratio' => FunderShareholdingRatio::create(
        $item->getLastInvestmentLog(),
        $item->getSumInvestmentLogs()
      ),
      'Investment Amount' => $array->get($item->getSumInvestmentLogs(), 'funder_investment_amount'),
      'Shareholding Quantity' => FunderShareholdingQuantity::create($item->getSumInvestmentLogs()),
      'Cost Per Share' => CostPerShare::create($item->getSumInvestmentLogs()),
      'Book Value Per Share' => $entity->get($item->getLastFinancialSummary(), 'book_value_per_share'),
      'Market Value Per Share' => $entity->get($item->getLastFinancialSummary(), 'market_value_per_share'),
      'Investment Baseline' => $entity->get($item->getLastFinancialSummary(), 'investment_baseline'),
      'Investment Gain' => InvestmentGain::create($item->getLastFinancialSummary(), $item->getSumInvestmentLogs()),
      'Capital Market Status' => $entity->get($item->getInvestmentTarget(), 'capital_market_status')
    ];
  }
}
