<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\EquityEvaluation;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Entity\Node;
use Drupal\site_custom\Helper\InvestmentReason;
use Drupal\site_custom\Report\DataSource\FinancialSummary;
use Drupal\site_custom\Report\DataSource\InvestmentLog;
use Drupal\site_custom\Report\DataSource\InvestmentProfile;
use RtLopez\Decimal;

/**
 * Class EquityEvaluationDataSource
 */
class EquityEvaluationDataSource
{
  protected $investmentTarget;

  protected $date;

  protected $investmentLogs = [];

  protected $sumInvestmentLogs = [];

  protected $lastInvestmentLog;

  protected $lastFinancialSummary;

  protected $lastInvestmentProfile;

  /**
   * @param EntityInterface $investmentTarget
   * @param DrupalDateTime  $date
   */
  function __construct(EntityInterface $investmentTarget, DrupalDateTime $date)
  {
    $this->investmentTarget = $investmentTarget;
    $this->date = $date;
    $this->investmentLogs = $this->loadInvestmentLogs();
    $this->sumInvestmentLogs = InvestmentReason::sum($this->investmentLogs);
    $this->lastInvestmentLog = $this->loadLastInvestmentLog();
    $this->lastFinancialSummary = $this->loadLastFinancialSummary();
    $this->lastInvestmentProfile = $this->loadLastInvestmentProfile();
  }

  /**
   * @return EntityInterface
   */
  function getInvestmentTarget(): EntityInterface
  {
    return $this->investmentTarget;
  }

  /**
   * @return DrupalDateTime
   */
  function getDate(): DrupalDateTime
  {
    return $this->date;
  }

  /**
   * @return EntityInterface[]
   */
  function getInvestmentLogs(): array
  {
    return $this->investmentLogs;
  }

  /**
   * @return Decimal[]
   */
  function getSumInvestmentLogs(): array
  {
    return $this->sumInvestmentLogs;
  }

  /**
   * @return EntityInterface
   */
  function getLastFinancialSummary(): EntityInterface
  {
    return $this->lastFinancialSummary;
  }

  /**
   * @return EntityInterface
   */
  function getLastInvestmentProfile(): EntityInterface
  {
    return $this->lastInvestmentProfile;
  }

  /**
   * @return EntityInterface
   */
  function getLastInvestmentLog()
  {
    return $this->lastInvestmentLog;
  }

  /**
   * @return EntityInterface[]
   */
  protected function loadInvestmentLogs(): array
  {
    $item = new InvestmentLog($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastInvestmentLog()
  {
    $items = $this->investmentLogs;

    return empty($items) ? Node\InvestmentLog::create()->getEmptyEntity() : reset($items);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastFinancialSummary()
  {
    $item = new FinancialSummary($this->investmentTarget);
    return $item->byDate($this->date);
  }

  /**
   * @return EntityInterface
   */
  protected function loadLastInvestmentProfile()
  {
    $item = new InvestmentProfile($this->investmentTarget);
    return $item->byDate($this->date);
  }
}
