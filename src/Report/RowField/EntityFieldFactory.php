<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField;

use Drupal\Core\Entity\EntityInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\site_custom\Helper\Field\Base\FieldBase;

/**
 * Class EntityFieldFactory
 */
class EntityFieldFactory extends RowFieldFactory
{
  protected $default = 'text';
  protected $route = [
    'text' => 'Drupal\site_custom\Helper\Field\Entity\Text',
    'entity_reference' => 'Drupal\site_custom\Helper\Field\Entity\EntityReference',
    'decimal' => 'Drupal\site_custom\Helper\Field\Entity\Decimal',
    'computed_decimal' => 'Drupal\site_custom\Helper\Field\Entity\Decimal',
    'datetime' => 'Drupal\site_custom\Helper\Field\Entity\DateTime',
    'integer' => 'Drupal\site_custom\Helper\Field\Entity\IntegerField',
    'list_string' => 'Drupal\site_custom\Helper\Field\Entity\TextList'
  ];

  /**
   * @param EntityInterface $item
   * @param string          $name
   * @param array           $format
   *
   * @return FieldBase
   */
  function get(EntityInterface $item, string $name, array $format = [])
  {
    $data = $item->getTypedData()->get($name);
    $type = $data->getDataDefinition()->getDataType();
    $index = $this->getFieldConfig($item, $name)->getType();
    $route = $this->route;

    if ('' === $data->getName()) {
      dpm($type);
    }

    $class = $route[$index] ?? $route[$this->default];
    return new $class($data);
  }

  /**
   * @param EntityInterface $item
   * @param string          $name
   *
   * @return FieldConfig
   */
  protected function getFieldConfig(EntityInterface $item, string $name): FieldConfig
  {
    $type = $item->getEntityTypeId();
    $bundle = $item->bundle();
    return FieldConfig::loadByName($type, $bundle, $name);
  }
}
