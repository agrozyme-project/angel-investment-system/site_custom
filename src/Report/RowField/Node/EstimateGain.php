<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use RtLopez\Decimal;

/**
 * Class EstimateGain
 */
class EstimateGain extends FloatNumber
{
  /**
   * @param EntityInterface $financialSummary
   * @param Decimal[]       $sumInvestmentLogs
   *
   * @return static
   */
  static function create(EntityInterface $financialSummary, array $sumInvestmentLogs)
  {
    //    $array = ArrayPropertyFactory::create();
    $evaluationValue = EvaluationValue::create($financialSummary, $sumInvestmentLogs)->getValue();
    $realizedGain = RealizedGain::create($sumInvestmentLogs)->getValue();
    $value = Decimal::create($evaluationValue)
      ->add($realizedGain)
      ->sub($sumInvestmentLogs['funder_investment_amount']);
    return new static($value);
  }
}
