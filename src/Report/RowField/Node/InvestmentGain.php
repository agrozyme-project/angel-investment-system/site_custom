<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Base\EntityList;
use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use RtLopez\Decimal;

/**
 * Class InvestmentGain
 */
class InvestmentGain extends FloatNumber
{
  /**
   * @param EntityInterface $financialSummary
   * @param Decimal[]       $sumInvestmentLogs
   *
   * @return InvestmentGain
   */
  static function create(EntityInterface $financialSummary, array $sumInvestmentLogs)
  {
    $field = EntityFieldFactory::create();

    /** @var EntityList $baseline */
    $baseline = $field->get($financialSummary, 'investment_baseline');
    $index = $baseline->getIndex();

    $quantity = FunderShareholdingQuantity::create($sumInvestmentLogs)->getValue();
    $cost = CostPerShare::create($sumInvestmentLogs)->getValue();

    switch ($index) {
      case 'market_value_per_share':
      case 'book_value_per_share':
        $gain = $field->get($financialSummary, $index)->getValue();
        break;

      case 'funder_cost_per_share':
      default:
        $gain = $cost;
        break;
    }

    $value = $gain->sub($cost)->mul($quantity);
    return new static($value);
  }
}
