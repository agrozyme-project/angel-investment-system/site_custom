<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\site_custom\Helper\Field\Value\IntegerNumber;
use RtLopez\Decimal;

/**
 * Class FunderShareholdingQuantity
 */
class FunderShareholdingQuantity extends IntegerNumber
{
  /**
   * @param Decimal[] $sumInvestmentLogs
   *
   * @return static
   */
  static function create(array $sumInvestmentLogs)
  {
    $value = Decimal::create($sumInvestmentLogs['funder_common_share'])->add(
      $sumInvestmentLogs['funder_preferred_share']
    );

    return new static($value);
  }
}
