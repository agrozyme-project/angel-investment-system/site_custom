<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\site_custom\Helper\Field\Value\IntegerNumber;
use RtLopez\Decimal;

/**
 * Class InvestorShareholdingQuantity
 */
class InvestorShareholdingQuantity extends IntegerNumber
{
  /**
   * @param Decimal[] $sumInvestmentLogs
   *
   * @return static
   */
  static function create(array $sumInvestmentLogs)
  {
    $value = Decimal::create($sumInvestmentLogs['investor_common_share'])->add(
      $sumInvestmentLogs['investor_preferred_share']
    );

    return new static($value);
  }
}
