<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use RtLopez\Decimal;

/**
 * Class RealizedGain
 */
class RealizedGain extends FloatNumber
{
  /**
   * @param Decimal[] $sumInvestmentLogs
   *
   * @return static
   */
  static function create(array $sumInvestmentLogs)
  {
    $value = Decimal::create(0)
      ->add($sumInvestmentLogs['funder_interest_income'])
      ->add($sumInvestmentLogs['funder_cash_dividend']);

    return new static($value);
  }
}
