<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Value\Text;
use Exception;

/**
 * Class Company
 */
class Company extends Text
{
  /**
   * @param EntityInterface $investmentTarget
   * @param EntityInterface $investmentProfile
   *
   * @return Text
   */
  static function create(EntityInterface $investmentTarget, EntityInterface $investmentProfile): Text
  {
    $value = '';

    $title = $investmentTarget
      ->getTypedData()
      ->get('title')
      ->getString();

    try {
      $items = $investmentProfile
        ->getTypedData()
        ->get('address')
        ->getValue();
      $address = reset($items);
      $value = isset($address['organization']) ? trim($address['organization']) : $title;
    } catch (Exception $exception) {
      $value = $title;
    }

    return new Text($value);
  }
}
