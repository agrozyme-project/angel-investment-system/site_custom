<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use RtLopez\Decimal;

/**
 * Class InvestorShareholdingRatio
 */
class InvestorShareholdingRatio extends FloatNumber
{
  /**
   * @param EntityInterface $investmentLog
   * @param Decimal[]       $sumInvestmentLogs
   *
   * @return static
   */
  static function create(EntityInterface $investmentLog, array $sumInvestmentLogs)
  {
    $value = Decimal::create(0);
    $field = EntityFieldFactory::create();

    /** @var Decimal $total */
    $total = $field->get($investmentLog, 'total_share')->getValue();

    if (false === $total->eq(0)) {
      $quantity = InvestorShareholdingQuantity::create($sumInvestmentLogs)->getValue();
      $value = $quantity->div($total);
    }

    return new static($value);
  }
}
