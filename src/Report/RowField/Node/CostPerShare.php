<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use Drupal\site_custom\Report\RowField\ArrayPropertyFactory;
use RtLopez\Decimal;

/**
 * Class CostPerShare
 */
class CostPerShare extends FloatNumber
{
  /**
   * @param Decimal[] $sumInvestmentLogs
   *
   * @return static
   */
  static function create(array $sumInvestmentLogs)
  {
    $value = Decimal::create(0);
    $array = ArrayPropertyFactory::create();
    $quantity = FunderShareholdingQuantity::create($sumInvestmentLogs)->getValue();

    if (false === $quantity->eq(0)) {
      $value = $sumInvestmentLogs['funder_investment_amount']->div($quantity);
    }

    return new static($value);
  }
}
