<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\Field\Value\FloatNumber;
use Drupal\site_custom\Report\RowField\EntityFieldFactory;
use RtLopez\Decimal;

/**
 * Class EvaluationValue
 */
class EvaluationValue extends FloatNumber
{
  /**
   * @param EntityInterface $financialSummary
   * @param Decimal[]       $sumInvestmentLogs
   *
   * @return static
   */
  static function create(EntityInterface $financialSummary, array $sumInvestmentLogs)
  {
    $field = EntityFieldFactory::create();
    $marketValuePerShare = $field->get($financialSummary, 'market_value_per_share')->getValue();

    $shareholdingQuantity = FunderShareholdingQuantity::create($sumInvestmentLogs)->getValue();

    $value = Decimal::create($marketValuePerShare)->mul($shareholdingQuantity);
    return new static($value);
  }
}
