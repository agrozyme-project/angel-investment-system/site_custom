<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField;

use Drupal\site_custom\Helper\Field\Base\FieldBase;
use RtLopez\Decimal;

/**
 * Class ArrayPropertyFactory
 */
class ArrayPropertyFactory extends RowFieldFactory
{
  protected $default = 'string';
  protected $route = [
    'string' => 'Drupal\site_custom\Helper\Field\Value\Text',
    'integer' => 'Drupal\site_custom\Helper\Field\Value\IntegerNumber',
    'double' => 'Drupal\site_custom\Helper\Field\Value\FloatNumber',
    'Decimal' => 'Drupal\site_custom\Helper\Field\Value\FloatNumber'
  ];

  /**
   * @param array  $item
   * @param string $name
   * @param array  $format
   *
   * @return FieldBase
   */
  function get(array $item, string $name, array $format = [])
  {
    $data = $item[$name] ?? '';

    if (is_scalar($data)) {
      $type = gettype($data);
      $class = $this->route[$type] ?? $this->route[$this->default];
    } elseif (is_array($data)) {
      $data = join("<br />\n", $data);
      $class = $this->route[$this->default];
    } elseif (is_object($data)) {
      $class = $this->getObjectClass($data);
    } else {
      $data = '';
      $class = $this->route[$this->default];
    }

    return new $class($data);
  }

  /**
   * @param $data
   *
   * @return string
   */
  protected function getObjectClass($data)
  {
    if ($data instanceof Decimal) {
      return $this->route['Decimal'];
    }

    return $this->route[$this->default];
  }
}
