<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\RowField;

/**
 * Class EntityFieldFactory
 */
class RowFieldFactory
{
  protected $route = [];

  /**
   * EntityFieldFactory constructor.
   */
  function __construct()
  {
  }

  /**
   * @return static
   */
  static function create()
  {
    return new static();
  }
}
