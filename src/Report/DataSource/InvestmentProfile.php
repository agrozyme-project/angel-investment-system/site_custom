<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\DataSource;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\site_custom\Helper\Entity\Node\InvestmentProfile as Entity;
use Drupal\site_custom\Report\DataSource\Base\InvestmentTargetBase;

/**
 * Class InvestmentProfile
 */
class InvestmentProfile extends InvestmentTargetBase
{
  /**
   * @inheritDoc
   */
  function __construct(EntityInterface $investmentTarget)
  {
    parent::__construct($investmentTarget);
    $this->entity = new Entity();
  }

  /**
   * @param DrupalDateTime $date
   *
   * @return EntityInterface
   */
  function byDate(DrupalDateTime $date)
  {
    $field = 'date';
    $format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    $query = $this->getLastQuery($field)->condition($field, $date->format($format), '<=');
    $ids = $query->execute();
    return $this->entity->getFirstOrEmptyEntity($ids);
  }
}
