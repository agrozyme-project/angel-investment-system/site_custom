<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\DataSource\Base;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\site_custom\Helper\Entity\EntityBase;

/**
 * Class InvestmentTargetBase
 */
abstract class InvestmentTargetBase
{
  /** @var EntityBase $entity */
  protected $entity;

  protected $investmentTarget;

  /**
   * @param EntityInterface $investmentTarget
   */
  function __construct(EntityInterface $investmentTarget)
  {
    $this->investmentTarget = $investmentTarget;
  }

  /**
   * @return QueryInterface
   */
  function getQuery()
  {
    return $this->entity->getQuery()->condition('investment_target', $this->investmentTarget->id(), '=');
  }

  /**
   * @param string $field
   *
   * @return QueryInterface
   */
  function getLastQuery(string $field)
  {
    return $this->getQuery()
      ->sort($field, 'DESC')
      ->range(0, 1);
  }
}
