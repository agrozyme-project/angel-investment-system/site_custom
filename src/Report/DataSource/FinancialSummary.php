<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\DataSource;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Helper\DatePeriod;
use Drupal\site_custom\Helper\Entity\Node\FinancialSummary as Entity;
use Drupal\site_custom\Report\DataSource\Base\InvestmentTargetBase;

/**
 * Class FinancialSummary
 */
class FinancialSummary extends InvestmentTargetBase
{
  /**
   * @inheritDoc
   */
  function __construct(EntityInterface $investmentTarget)
  {
    parent::__construct($investmentTarget);
    $this->entity = new Entity();
  }

  /**
   * @param DrupalDateTime $date
   *
   * @return EntityInterface
   */
  function byDate(DrupalDateTime $date)
  {
    $item = new DatePeriod($date);
    $query = $this->getQuery()
      ->condition('year', $item->getYear(), '=')
      ->condition('quarter', $item->getQuarter(), '=');

    $ids = $query->execute();
    return $this->entity->getFirstOrEmptyEntity($ids);
  }
}
