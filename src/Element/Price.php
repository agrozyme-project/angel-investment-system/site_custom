<?php
declare(strict_types=1);

namespace Drupal\site_custom\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\price\Element\Price as Base;

/**
 * Class Price
 */
class Price extends Base
{
  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * @{@inheritDoc}
   */
  static function processElement(array $element, FormStateInterface $form_state, array &$complete_form)
  {
    $options = &$element['currency_code']['#options'];

    if (isset($options)) {
      $options = static::sort($options);
    }

    return $element;
  }

  /**
   * @param array $items
   *
   * @return array
   */
  protected static function sort(array $items): array
  {
    $index = 'TWD';
    $up = isset($items[$index]) ? [$index => $items[$index]] : [];
    unset($items[$index]);
    return $up + $items;
  }
}
