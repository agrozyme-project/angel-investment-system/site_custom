<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Filter\Base\FilterItemBase;
use Drupal\site_custom\TaiwanRegion;
use Tightenco\Collect\Support\Collection;

/**
 * Class AnalysisType
 */
class AnalysisType extends FilterItemBase
{
  /**
   * @param string $key
   */
  function __construct(string $key)
  {
    parent::__construct($key);
    $this->value = $this->query(null);
  }

  /**
   * @return array
   */
  function element(): array
  {
    return [
      '#type' => 'select',
      '#title' => $this->t('Analysis Type'),
      '#options' => $this->getOptions(),
      '#default_value' => $this->value,
      '#required' => true,
      '#chosen' => true
    ];
  }

  /**
   * @return Collection
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getItem()
  {
    $data = Collection::make([]);

    switch ($this->value) {
      case 'industry_type':
      case 'company_type':
      case 'company_stage':
      case 'fundraising_stage':
        $data = $this->loadTerms($this->value);
        break;

      case 'region':
        $data = $this->loadRegion();
        break;

      default:
        break;
    }

    return $data;
  }

  /**
   * @return string
   */
  function getLabel(): string
  {
    $options = $this->getOptions();
    return $options[$this->value] ?? '';
  }

  /**
   * @return array
   */
  protected function getOptions()
  {
    return [
      'industry_type' => t('Industry Type'),
      'region' => t('Region'),
      'company_type' => t('Company Type'),
      'company_stage' => t('Company Stage'),
      'fundraising_stage' => t('Fundraising Stage')
    ];
  }

  /**
   * @param string $bundle
   *
   * @return Collection
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function loadTerms(string $bundle): Collection
  {
    $type = 'taxonomy_term';
    $items = Drupal::entityTypeManager()
      ->getStorage($type)
      ->loadByProperties(['type' => $bundle]);

    return Collection::make($items)->filter(function ($value, $key) use ($bundle) {
      /**
       * @var EntityInterface $value
       */
      return $bundle === $value->bundle();
    });
  }

  /**
   * @return Collection
   */
  protected function loadRegion()
  {
    $items = TaiwanRegion::getInstance()->getNames();
    return Collection::make($items);
  }
}
