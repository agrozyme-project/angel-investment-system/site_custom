<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\site_custom\Filter\Base\FilterItemBase;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentType
 */
class InvestmentTypes extends FilterItemBase
{
  /**
   * @param string $key
   */
  function __construct(string $key)
  {
    parent::__construct($key);
    $this->value = $this->query([]);
  }

  /**
   * @inheritDoc
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function element(): array
  {
    return [
      '#type' => 'select',
      '#title' => $this->t('Investment Type'),
      '#options' => $this->getInvestmentTypeOptions(),
      //      '#empty_option' => $this->t('- None -'),
      //      '#empty_value' => null,
      '#multiple' => true,
      '#default_value' => $this->value,
      '#required' => false,
      '#chosen' => true
    ];
  }

  /**
   * @return Collection
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getItem()
  {
    $type = 'taxonomy_term';
    $bundle = 'investment_type';
    $storage = Drupal::entityTypeManager()->getStorage($type);
    $keys = array_keys($this->value);
    $items = empty($keys) ? [] : $storage->loadMultiple($keys);

    return Collection::make($items)->filter(function ($value, $key) use ($bundle) {
      /**
       * @var EntityInterface $value
       */
      return $bundle === $value->bundle();
    });
  }

  /**
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getInvestmentTypeOptions()
  {
    $items = Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'investment_type']);

    $langcode = Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    return Collection::make($items)
      ->map(function (EntityInterface $item) use ($langcode) {
        /** @var EntityInterface $term */
        $term = Drupal::service('entity.repository')->getTranslationFromContext($item, $langcode);
        return $term->label();
      })
      ->toArray();
  }
}
