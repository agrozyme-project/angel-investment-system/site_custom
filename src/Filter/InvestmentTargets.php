<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\site_custom\Filter\Base\FilterItemBase;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentTargets
 */
class InvestmentTargets extends FilterItemBase
{
  /**
   * @param string $key
   */
  function __construct(string $key)
  {
    parent::__construct($key);
    $this->value = $this->query([]);
  }

  /**
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function element(): array
  {
    return [
      '#type' => 'select',
      '#title' => $this->t('Investment Target'),
      '#options' => $this->getInvestmentTargetOptions(),
      '#multiple' => true,
      '#default_value' => $this->value,
      '#chosen' => true
    ];
  }

  /**
   * @return Collection
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getItem()
  {
    $type = 'node';
    $bundle = 'investment_target';
    $storage = Drupal::entityTypeManager()->getStorage($type);
    $keys = array_keys($this->value);
    $items = empty($keys) ? $storage->loadByProperties(['type' => $bundle]) : $storage->loadMultiple($keys);

    return Collection::make($items)->filter(function ($value, $key) use ($bundle) {
      /**
       * @var EntityInterface $value
       */
      return $bundle === $value->bundle();
    });
  }

  /**
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getInvestmentTargetOptions()
  {
    $items = Drupal::entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'investment_target']);

    return Collection::make($items)
      ->map(function (EntityInterface $item) {
        return $item->label();
      })
      ->toArray();
  }
}
