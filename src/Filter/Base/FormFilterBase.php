<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter\Base;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Tightenco\Collect\Support\Collection;

/**
 * Class FilterBase
 */
abstract class FormFilterBase
{
  use StringTranslationTrait;

  /**
   * @var Collection
   */
  protected $items;

  /**
   * FilterBase constructor.
   */
  function __construct()
  {
    $this->items = $this->setupItems();
  }

  /**
   * @return Collection
   */
  function getItems()
  {
    return $this->items;
  }

  /**
   * @return array
   */
  function build()
  {
    $items = $this->getItems()
      ->map(function ($item, $key) {
        /**
         * @var FilterItemBase $item
         */
        return $item->element();
      })
      ->toArray();

    return ['#type' => 'details', '#title' => $this->t('Filters')] + $items;
  }

  /**
   * @return Collection
   */
  abstract protected function setupItems(): Collection;
}
