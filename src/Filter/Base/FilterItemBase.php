<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter\Base;

use Drupal;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class FilterItemBase
 */
abstract class FilterItemBase
{
  use StringTranslationTrait;

  protected $key;

  protected $value;

  /**
   * @param string $key
   */
  function __construct(string $key)
  {
    $this->key = $key;
  }

  function getValue()
  {
    return $this->value;
  }

  /**
   * @return array
   */
  abstract function element(): array;

  abstract function getItem();

  /**
   * @param $default
   *
   * @return mixed
   */
  protected function query($default)
  {
    return Drupal::request()->query->get($this->key, $default);
  }
}
