<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter\Base;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Tightenco\Collect\Support\Collection;

/**
 * Class FilterBase
 */
abstract class FilterBase
{
  use StringTranslationTrait;

  /**
   * @var Collection
   */
  protected $items;

  /**
   * FilterBase constructor.
   */
  function __construct()
  {
    $this->items = $this->setupItems();
  }

  /**
   * @return Collection
   */
  function getItems()
  {
    return $this->items;
  }

  /**
   * @return Collection
   */
  abstract protected function setupItems(): Collection;
}
