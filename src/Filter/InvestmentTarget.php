<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Tightenco\Collect\Support\Collection;

/**
 * Class InvestmentTarget
 */
class InvestmentTarget extends InvestmentTargets
{
  /**
   * @param string $key
   */
  function __construct(string $key)
  {
    parent::__construct($key);
    $this->value = $this->query(null);
  }

  /**
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function element(): array
  {
    return ['#multiple' => false, '#required' => true] + parent::element();
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * @return Collection
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  function getItem()
  {
    $type = 'node';
    $bundle = 'investment_target';
    $storage = Drupal::entityTypeManager()->getStorage($type);
    $keys = array_keys($this->value);
    $items = empty($keys) ? [] : $storage->loadMultiple($keys);

    return Collection::make($items)->filter(function ($value, $key) use ($bundle) {
      /**
       * @var EntityInterface $value
       */
      return $bundle === $value->bundle();
    });
  }
}
