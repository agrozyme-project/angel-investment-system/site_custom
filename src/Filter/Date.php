<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\site_custom\Filter\Base\FilterItemBase;

/**
 * Class Date
 */
class Date extends FilterItemBase
{
  /**
   * @param string $key
   */
  function __construct(string $key)
  {
    parent::__construct($key);
    $this->value = $this->query('');
  }

  /**
   * @return array
   */
  function element(): array
  {
    return [
      '#type' => 'date',
      '#title' => $this->t('Date'),
      '#default_value' => $this->value
    ];
  }

  /**
   * @return DrupalDateTime
   */
  function getItem()
  {
    return new DrupalDateTime($this->value);
  }
}
