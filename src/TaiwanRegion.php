<?php
declare(strict_types=1);

namespace Drupal\site_custom;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class TaiwanRegion
 */
class TaiwanRegion
{
  protected static $instance = null;
  protected $names = [];
  protected $divisions = [];
  protected $regions = [];

  protected function __construct()
  {
    $this->names = $this->setupNames();
    $this->divisions = $this->setupDivisions();
    $this->regions = $this->setupRegions();
  }

  /**
   * @return static
   */
  static function getInstance()
  {
    if (false == static::$instance) {
      static::$instance = new static();
    }

    return static::$instance;
  }

  /**
   * @return TranslatableMarkup[]
   */
  function getNames()
  {
    return $this->names;
  }

  /**
   * @param string $index
   *
   * @return TranslatableMarkup
   */
  function getName(string $index)
  {
    return $this->names[$index] ?? t('');
  }

  /**
   * @param array $address
   *
   * @return string
   */
  function getRegion($address)
  {
    if ('TW' !== $address['country_code']) {
      return 'Overseas';
    }

    $index = $address['administrative_area'];

    if (isset($this->regions[$index])) {
      return $this->regions[$index];
    }

    return '';
  }

  /**
   * @return TranslatableMarkup[]
   */
  protected function setupNames()
  {
    $options = ['context' => 'Address label'];
    return [
      'North' => t('North', [], $options),
      'Central' => t('Central', [], $options),
      'South' => t('South', [], $options),
      'East' => t('East', [], $options),
      'Island' => t('Island', [], $options),
      'Overseas' => t('Overseas', [], $options)
    ];
  }

  /**
   * @return string[][]
   */
  protected function setupDivisions()
  {
    $items = [];
    $items['North'] = [
      'Taipei City',
      'Yilan County',
      'Taoyuan City',
      'Keelung City',
      'New Taipei City',
      'Hsinchu City',
      'Hsinchu County'
    ];
    $items['Central'] = ['Taichung City', 'Nantou County', 'Miaoli County', 'Yunlin County', 'Changhua County'];
    $items['South'] = ['Tainan City', 'Pingtung County', 'Kaohsiung City', 'Chiayi City', 'Chiayi County'];
    $items['East'] = ['Taitung County', 'Hualien County'];
    $items['Island'] = ['Kinmen County', 'Lienchiang County', 'Penghu County'];
    return $items;
  }

  /**
   * @return string[]
   */
  protected function setupRegions()
  {
    $items = [];

    foreach ($this->divisions as $name => $divisions) {
      foreach ($divisions as $division) {
        $items[$division] = $name;
      }
    }

    return $items;
  }
}
