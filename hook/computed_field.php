<?php
declare(strict_types=1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\FieldItemInterface;
use function hook_computed_field_dispatcher_computed_field_compute as compute;
use function hook_computed_field_dispatcher_computed_field_format as format;

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_shareholding_ratio_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'shareholding_ratio';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_shareholding_ratio_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_gross_profit_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'gross_profit';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_gross_profit_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_operating_income_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'operating_income';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_operating_income_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_gross_margin_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'gross_margin';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_gross_margin_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_net_profit_margin_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'net_profit_margin';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_net_profit_margin_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_total_equity_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'total_equity';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_total_equity_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_current_ratio_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'current_ratio';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_current_ratio_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_debt_ratio_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'debt_ratio';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_debt_ratio_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_earning_per_share_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'earning_per_share';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_earning_per_share_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_region_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'region';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_region_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_funder_subtotal_share_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'funder_subtotal_share';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_funder_subtotal_share_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_funder_shareholding_ratio_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'funder_shareholding_ratio';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_funder_shareholding_ratio_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_funder_cost_per_share_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'funder_cost_per_share';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_funder_cost_per_share_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_investor_subtotal_share_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'investor_subtotal_share';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_investor_subtotal_share_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_investor_shareholding_ratio_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'investor_shareholding_ratio';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_investor_shareholding_ratio_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_investor_cost_per_share_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'investor_cost_per_share';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_investor_cost_per_share_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_book_value_per_share_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'book_value_per_share';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_book_value_per_share_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}
